import csv
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

from datetime import datetime, date, time, timezone, timedelta
import pandas as pd

def read_log(filename):
    the_list = []
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            the_list.append([el.strip() for el in row])
    return the_list

def mkdatetimems(t):
    base_datetime = datetime(2021, 12, 13)
    delta = timedelta(hours=11, minutes=35, milliseconds=int(t))
    return base_datetime + delta

# takes string like "01/01/22 12:00:00"
def mkdatetime(t):
    ds,ts = t.split(" ")
    ds=ds.split("/")
    ts=ts.split(":")
    ds=[int(s) for s in ds]
    ts=[int(s) for s in ts]
    d=date(ds[0],ds[1],ds[2])
    t=time(ts[0],ts[1],ts[2])
    return datetime.combine(d,t)

def parse_log(log):
    pm_list=[]
    no2_list=[]
    start = mkdatetime(log[0][0])+timedelta(hours=1,minutes=30)
    for l in log:
        if len(l)>3:
            t = mkdatetime(l[0])
            if t>start:
                if l[2]=="pm":
                    if float(l[3])>0 and float(l[4])>0 and float(l[5])>0 and float(l[3])<60000 and float(l[4])<60000 and float(l[5])<60000:
                        pm_list.append(dict(time=t,
                                            pm1=float(l[3]),
                                            pm25=float(l[4]),
                                            pm10=float(l[5])))

                if l[2]=="no2" or l[2]=="n02":
                    if float(l[6])>0:
                        no2_list.append(dict(time=t,
                                             temp=float(l[3]),
                                             ppb=float(l[6])))

    return pd.merge(pd.DataFrame(no2_list),
                    pd.DataFrame(pm_list),
                    how="outer",on=["time"])

app = dash.Dash(__name__)

app.layout = html.Div([
    html.H1("SMOGOFF data repository"),
    html.P("Air quality measurements in Cornwall"),
    dcc.Dropdown(
        id="smoothing",
        options=[{'label': '50', 'value': 50},
                 {'label': '100', 'value': 100}],
        value=50,
        clearable=False),
    dcc.Dropdown(
        id="dropdown",
        options=[
            {'label': x, 'value': x}
            for x in ['013-2020-12-20-B',
                      '013-2021-12-20-A',
                      '014-2021-12-21-B',
                      '014-2021-12-21-A'
            ]
        ],
        value='013-2020-12-20-B',
        clearable=False,
    ),
    dcc.Graph(id="graph"),
])

path = "U:\\GitHub\\SmogOff\\data\\smartlinedata\\"
basename = "\\DATALOG.TXT"

@app.callback(
    Output("graph", "figure"),
    [Input("dropdown", "value"),
     Input("smoothing", "value")])
def display_graph(filename,smoothing):
    df = parse_log(read_log(path+filename+basename))
    print(df)
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    #fig.add_trace(go.Scatter(x=df["time"],y=df["pm1"], name="PM1.0 ug/m3"), secondary_y=False)
    fig.add_trace(go.Scatter(x=df["time"],y=df["pm25"], name="PM2.5 ug/m3", mode="markers", marker=dict(color="#f18f01", size=2)), secondary_y=False)
    fig.add_trace(go.Scatter(x=df["time"],y=df["pm25"].rolling(smoothing).mean(), name="PM2.5 ug/m3 avg", line=dict(color="#f18f01")), secondary_y=False)
    #fig.add_trace(go.Scatter(x=df["time"],y=df["pm10"], name="PM10.0 ug/m3"), secondary_y=False)
    fig.add_trace(go.Scatter(x=df["time"],y=df["ppb"], name="N02 ppb", mode="markers", marker=dict(color="#a04668", size=2)), secondary_y=True)
    fig.add_trace(go.Scatter(x=df["time"],y=df["ppb"].rolling(smoothing).mean(), name="N02 ppb avg", line=dict(color="#a04668")), secondary_y=True)
    fig.add_trace(go.Scatter(x=df["time"],y=df["temp"], name="Temp C", line=dict(color="#29bf12")), secondary_y=False)
    return fig


app.run_server(debug=True)
