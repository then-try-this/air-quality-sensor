import csv
import matplotlib.pyplot as plt
import numpy as np

def read_log(file):
    the_list = []
    with open(file, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            the_list.append([el.strip() for el in row])
    return(the_list)

def parse_log(log):
    n02 = []
    pm = []
    for l in log:
        if len(l)>3:
            t = l[0]
            if l[2]=="pm":
                if float(l[3])>0 and float(l[4])>0 and float(l[5])>0:
                    pm.append({
                        "time": t,
                        "pm1": float(l[3]),
                        "pm25": float(l[4]),
                        "pm10": float(l[5])
                    })
            if l[2]=="n02":
                n02.append({
                    "time": t,
                    "temp": float(l[3]),
                    "ppb": float(l[6])
                })
    return [n02,pm]

def parse_log2(log):
    n02 = []
    pm = []
    for l in log:
        if len(l)>3:
            t = l[0]
            if l[1]=="pm":
                pm.append({
                    "time": t,
                    "pm1": float(l[2]),
                    "pm25": float(l[3]),
                    "pm10": float(l[4])
                })
            if l[1]=="n02":
                n02.append({
                    "time": t,
                    "temp": float(l[2]),
                    "ppb": float(l[5])
                })
    return [n02,pm]

filenameA = "/home/dave/projects/smogoff/data/002-garden-test2-A/DATALOG.TXT"
filenameB = "/home/dave/projects/smogoff/data/003-garden-test2-B/DATALOG.TXT"

n02,pm = parse_log(read_log(filenameA))
#n02,pm = parse_log2(read_log(filenameB))

for i in pm:
    print(i)

# plot
fig, ax = plt.subplots()

ax.plot([el["pm25"] for el in pm],color="blue")
#ax.plot([el["pm10"] for el in pm])
#ax2=ax.twinx()
#ax2.plot([el["ppb"] for el in n02][50:-50],color="red")

plt.show()

