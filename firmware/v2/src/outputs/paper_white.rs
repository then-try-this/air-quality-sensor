use embedded_graphics::{
    image::Image,
    mono_font::{ascii::*, MonoTextStyle},
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{PrimitiveStyle, Rectangle},
    text::{Alignment, Text},
};
use embedded_text::{
    alignment::HorizontalAlignment,
    style::{HeightMode, TextBoxStyleBuilder},
    TextBox,
};

use tinybmp::Bmp;
use uc8151::*;

use embedded_hal::digital::InputPin;
use embedded_hal::digital::OutputPin;
use embedded_hal::spi::SpiDevice;

static NOOD_IMG: &[u8; 2194] = include_bytes!("../../images/nood_1bpp.bmp");
static TTT_IMG: &[u8; 2034] = include_bytes!("../../images/thentrythis.bmp");
static SMILE_IMG: &[u8; 2194] = include_bytes!("../../images/smileface.bmp");
static NEUTRAL_IMG: &[u8; 2194] = include_bytes!("../../images/neutralface.bmp");
static SAD_IMG: &[u8; 2194] = include_bytes!("../../images/sadface.bmp");

pub enum ScreenState {
    Title,
    Face,
    Data,
}

pub struct PaperWhite<SPI, DC, BUSY, RESET> {
    display: Uc8151<SPI, DC, BUSY, RESET>,
}

impl<SPI, DC, BUSY, RESET> PaperWhite<SPI, DC, BUSY, RESET>
where
    SPI: SpiDevice,
    DC: OutputPin,
    BUSY: InputPin,
    RESET: OutputPin,
{
    pub fn new(display: Uc8151<SPI, DC, BUSY, RESET>) -> Self {
        PaperWhite { display }
    }

    pub fn print(&mut self, text: &str) {
        let style = MonoTextStyle::new(&FONT_6X10, BinaryColor::Off);

        Rectangle::new(Point::new(0, 0), Size::new(WIDTH, 16))
            .into_styled(PrimitiveStyle::with_fill(BinaryColor::On))
            .draw(&mut self.display)
            .unwrap();

        Text::with_alignment(text, Point::new(10, 10), style, Alignment::Left)
            .draw(&mut self.display)
            .unwrap();

        let _ = self
            .display
            .partial_update(UpdateRegion::new(0, 0, WIDTH, 16).unwrap());
    }

    pub fn draw_stuff(&mut self) {
        // Note we're setting the Text color to `Off`. The driver is set up to treat Off as Black so that BMPs work as expected.
        let character_style = MonoTextStyle::new(&FONT_9X18_BOLD, BinaryColor::Off);
        let textbox_style = TextBoxStyleBuilder::new()
            .height_mode(HeightMode::FitToText)
            .alignment(HorizontalAlignment::Center)
            .paragraph_spacing(6)
            .build();

        // Bounding box for our text. Fill it with the opposite color so we can read the text.
        let bounds = Rectangle::new(Point::new(157, 10), Size::new(WIDTH - 157, 0));
        bounds
            .into_styled(PrimitiveStyle::with_fill(BinaryColor::On))
            .draw(&mut self.display)
            .unwrap();

        // Create the text box and apply styling options.
        let text = "We\nLove\nNOOOoooOOOOOD!";
        let text_box = TextBox::with_textbox_style(text, bounds, character_style, textbox_style);

        // Draw the text box.
        text_box.draw(&mut self.display).unwrap();

        let _ = self.display.update();

        let tga: Bmp<BinaryColor> = Bmp::from_slice(TTT_IMG).unwrap();
        let image = Image::new(&tga, Point::zero());
        let _ = image.draw(&mut self.display);
        let _ = self.display.update();
    }

    pub fn draw(&mut self, state: ScreenState) {
        // Note we're setting the Text color to `Off`. The driver is set up to treat Off as Black so that BMPs work as expected.
        let character_style = MonoTextStyle::new(&FONT_9X18_BOLD, BinaryColor::Off);
        let textbox_style = TextBoxStyleBuilder::new()
            .height_mode(HeightMode::FitToText)
            .alignment(HorizontalAlignment::Left)
            .paragraph_spacing(6)
            .build();

        // Bounding box for our text. Fill it with the opposite color so we can read the text.
        let bounds = Rectangle::new(Point::new(157, 10), Size::new(WIDTH - 157, 0));
        bounds
            .into_styled(PrimitiveStyle::with_fill(BinaryColor::On))
            .draw(&mut self.display)
            .unwrap();

        // Create the text box and apply styling options.
        let text = "SmogOff Air Pollution Sensor thentrythis.org";
        let text_box = TextBox::with_textbox_style(text, bounds, character_style, textbox_style);

        // Draw the text box.
        text_box.draw(&mut self.display).unwrap();

        match state {
            ScreenState::Title => {
                let tga: Bmp<BinaryColor> = Bmp::from_slice(TTT_IMG).unwrap();
                let image = Image::new(&tga, Point::zero());
                let _ = image.draw(&mut self.display);
            }
            _ => {}
        }

        let _ = self.display.update();
    }
}
