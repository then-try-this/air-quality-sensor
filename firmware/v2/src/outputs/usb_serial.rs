// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rp2040_hal::usb::UsbBus;
use usb_device::class_prelude::UsbBusAllocator;
use usb_device::prelude::{StringDescriptors, UsbDevice, UsbDeviceBuilder, UsbVidPid};
use usbd_serial::{SerialPort, USB_CLASS_CDC};

/// Allows debugging information to be sent via USB connection
pub struct USBSerial<'a> {
    pub serial: SerialPort<'a, UsbBus>,
    pub usb_dev: UsbDevice<'a, UsbBus>,
}

impl<'a> USBSerial<'_> {
    pub fn new(usb_bus: &'a UsbBusAllocator<UsbBus>) -> USBSerial<'a> {
        // Set up the USB Communications Class Device driver
        let serial = SerialPort::new(&usb_bus);

        // Create a USB device with a fake VID and PID
        let usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
            .strings(&[StringDescriptors::default()
                .manufacturer("Then Try This")
                .product("SmogOff")
                .serial_number("1")])
            .unwrap()
            .device_class(USB_CLASS_CDC)
            .build();

        USBSerial { serial, usb_dev }
    }

    pub fn log(&mut self, text: &str) {
        let _ = self.serial.write(text.as_bytes());
    }
}
