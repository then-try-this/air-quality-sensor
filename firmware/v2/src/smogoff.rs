// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#![no_std]
#![no_main]
//#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_mut)]

extern crate cortex_m;
extern crate embedded_ads111x;
extern crate embedded_graphics;
extern crate embedded_graphics_core;
extern crate embedded_hal;
extern crate embedded_hal_bus;
extern crate embedded_sdmmc;
extern crate embedded_text;
extern crate heapless;
extern crate onewire;
extern crate panic_halt;
extern crate rp2040_hal;
extern crate tinybmp;
extern crate uc8151;
extern crate usb_device;
extern crate usbd_serial;

// Ensure we halt the program on panic (if we don't mention this crate it won't
// be linked)
#[allow(unused_imports)]
use panic_halt as _;

use core::cell::RefCell;
use hal::pac;
use rp2040_hal as hal;

use rp2040_hal::{
    clocks::Clock,
    fugit::RateExtU32,
    gpio::{FunctionI2C, Pin, PullUp},
    usb::UsbBus,
};

// various busses we need (and might have to share)
use core::fmt::Write;
use embedded_hal::digital::OutputPin;
use embedded_hal_bus::i2c;
use embedded_hal_bus::spi;
use heapless::String;
use onewire::*;
use usb_device::class_prelude::UsbBusAllocator;

use embedded_sdmmc::*;

#[derive(Default)]
pub struct DummyTimesource();

impl TimeSource for DummyTimesource {
    // In theory you could use the RTC of the rp2040 here, if you had
    // any external time synchronizing device.
    fn get_timestamp(&self) -> Timestamp {
        Timestamp {
            year_since_1970: 0,
            zero_indexed_month: 0,
            zero_indexed_day: 0,
            hours: 0,
            minutes: 0,
            seconds: 0,
        }
    }
}

/////////////////////////////////////////////////////////////
// smogoff stuff

pub mod sensors;
use sensors::no2_sensor::*;
use sensors::pcf8523::*;
use sensors::pm_sensor::*;
use sensors::thermometer::*;
pub mod outputs;
use outputs::paper_white::*;
use outputs::usb_serial::USBSerial;

#[derive(Debug, Copy, Clone, PartialEq)]
enum SmogOffState {
    PMStart,
    PMWarmup,
    PMRead,
    PMStop,
    PMWarmdown,
    NO2Read,
    WriteData,
    Idle,
}

#[derive(Debug)]
enum SmogOffError {
    None,
    PMMissing,
    SDCard,
    PMNoData,
    RTCInit,
    RTCRead,
    NoTemp,
}

////////////////////////////////////////////////////////////////////////////////////////

#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER_GENERIC_03H;

/// External high-speed crystal on the Raspberry Pi Pico board is 12 MHz. Adjust
/// if your board has a different frequency
const XTAL_FREQ_HZ: u32 = 12_000_000u32;

const PM_READ_DURATION: u32 = 10;
const PM_WARMUP_DURATION: u32 = 3;
const PM_WARMDOWN_DURATION: u32 = 1;
const PM_MAX_ERRORS: u32 = 50; // 50 read fails @ 100ms per attempt
const NO2_READ_DURATION: u32 = 10;
const SAMPLING_PERIOD: u32 = 5; //60;
const NO2_SAMPLE_PERIOD: u32 = 1;

////////////////////////////////////////////////////////////

#[rp2040_hal::entry]
fn main() -> ! {
    // Grab our singleton objects
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    // Set up the watchdog driver - needed by the clock setup code
    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    // Configure the clocks
    let clocks = hal::clocks::init_clocks_and_plls(
        XTAL_FREQ_HZ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let smogoff_error = SmogOffError::None;

    // clocks
    let mut timer = hal::Timer::new(pac.TIMER, &mut pac.RESETS, &clocks);
    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    // The single-cycle I/O block controls our GPIO pins
    let sio = hal::Sio::new(pac.SIO);

    // Set the pins to their default state
    let pins = hal::gpio::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    // Configure GPIO25 as an output
    let mut led_pin = pins.gpio25.into_push_pull_output();
    led_pin.set_low().unwrap();

    // set up our shared I2C bus
    let sda_pin: Pin<_, FunctionI2C, PullUp> = pins.gpio4.reconfigure();
    let scl_pin: Pin<_, FunctionI2C, PullUp> = pins.gpio5.reconfigure();

    let mut i2c = hal::I2C::i2c0(
        pac.I2C0,
        sda_pin,
        scl_pin,
        400.kHz(),
        &mut pac.RESETS,
        &clocks.system_clock,
    );

    let ri2c = RefCell::new(i2c);

    // set up our shared SPI bus
    let spi_mosi = pins.gpio19.into_function::<hal::gpio::FunctionSpi>();
    let spi_miso = pins.gpio16.into_function::<hal::gpio::FunctionSpi>();
    let spi_sclk = pins.gpio18.into_function::<hal::gpio::FunctionSpi>();
    let spi = hal::spi::Spi::<_, _, _, 8>::new(pac.SPI0, (spi_mosi, spi_miso, spi_sclk));

    // Exchange the uninitialised SPI driver for an initialised one
    let mut spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        1.MHz(),
        embedded_hal::spi::MODE_0,
    );

    let rspi = RefCell::new(spi);

    //////////////////////////////////////////////////////////////////////
    // usb serial

    let usb_bus = UsbBusAllocator::new(UsbBus::new(
        pac.USBCTRL_REGS,
        pac.USBCTRL_DPRAM,
        clocks.usb_clock,
        true,
        &mut pac.RESETS,
    ));

    let mut usb_serial = USBSerial::new(&usb_bus);

    //////////////////////////////////////////////////////////////////////
    // PM sensor

    let mut pm_sensor = PMSensor::new(
        (pins.gpio0.into_function(), pins.gpio1.into_function()),
        pac.UART0,
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
    );

    pm_sensor.sleep(&mut usb_serial);

    //////////////////////////////////////////////////////////////////////
    // one wire thermometer

    let mut one = pins.gpio2.into_push_pull_output();
    let mut wire = OneWire::new(&mut one, false);
    let mut thermometer = Thermometer::new();

    ///////////////////////////////////////////////////////////////////////
    // NO2 sensor

    let i2cd = i2c::RefCellDevice::new(&ri2c);
    let mut no2_sensor = NO2Sensor::new(i2cd);

    ///////////////////////////////////////////////////////////////////////
    // RTC

    let i2cd2 = i2c::RefCellDevice::new(&ri2c);
    let mut rtc = Pcf8523::new(i2cd2);

    //////////////////////////////////////////////////////////////////////
    // screen

    let mut screen_dc = pins.gpio20.into_push_pull_output();
    let mut screen_cs = pins.gpio14.into_push_pull_output();
    let screen_busy = pins.gpio26.into_pull_up_input();
    let screen_reset = pins.gpio21.into_push_pull_output();
    let spid = spi::RefCellDevice::new(&rspi, screen_cs, timer).unwrap();

    //////////////////////////////////////////////////////////////////////
    // sdcard

    //let mut sd_cs = pins.gpio14.into_push_pull_output();
    let mut sd_cs = pins.gpio17.into_push_pull_output();

    // "Before talking to the SD Card, the caller needs to send 74 clocks cycles on
    // the SPI Clock line, at 400 kHz, with no chip-select asserted (or at least,
    // not the chip-select of the SD Card)."
    sd_cs.set_high().unwrap();
    delay.delay_ms(500);

    let sd_spid = spi::RefCellDevice::new(&rspi, sd_cs, timer).unwrap();
    let sdcard = embedded_sdmmc::SdCard::new(sd_spid, timer);
    let mut volume_mgr = VolumeManager::new(sdcard, DummyTimesource::default());

    match volume_mgr.device().num_bytes() {
        Ok(bytes) => {
            led_pin.set_high().unwrap();
            let mut volume = volume_mgr
                .open_volume(embedded_sdmmc::VolumeIdx(0))
                .unwrap();
            led_pin.set_low().unwrap();
        }
        Err(e) => {
            //screen.print("no sdcard");
        }
    }

    //////////////////////////////////////////////////////////////////////
    // main loop

    let mut next_update_time = 1_000_000;
    let mut state = SmogOffState::WriteData;
    let mut timer_secs = 0;
    let mut sampling_secs = 0;
    let mut last_state = state;

    let mut display = uc8151::Uc8151::new(spid, screen_dc, screen_busy, screen_reset);
    let screen_state = display.setup(&mut delay, uc8151::LUT::Internal);
    display.reset(&mut delay);
    let mut screen = PaperWhite::new(display);
    screen.draw(ScreenState::Title);

    loop {
        // update every second
        if timer.get_counter().ticks() >= next_update_time {
            /*{
                let mut text_buffer: String<64> = String::new();
                writeln!(&mut text_buffer, "{:?}\n", rtc.get_time()).unwrap();
                usb_serial.log(&text_buffer);
            }*/

            match state {
                SmogOffState::PMStart => {
                    usb_serial.log("waking...\n");
                    pm_sensor.wake();
                    state = SmogOffState::PMWarmup;
                    timer_secs = 0;
                }

                SmogOffState::PMWarmup => {
                    if timer_secs > PM_WARMUP_DURATION {
                        state = SmogOffState::PMRead;
                        timer_secs = 0;
                    }
                }

                SmogOffState::PMRead => {
                    pm_sensor.update(&mut usb_serial);
                    if timer_secs > PM_READ_DURATION {
                        state = SmogOffState::PMStop;
                        timer_secs = 0;
                    }
                }

                SmogOffState::PMStop => {
                    usb_serial.log("sleeping...\n");
                    pm_sensor.sleep(&mut usb_serial);
                    state = SmogOffState::PMWarmdown;
                }

                SmogOffState::PMWarmdown => {
                    if timer_secs > PM_WARMDOWN_DURATION {
                        state = SmogOffState::NO2Read;
                        timer_secs = 0;
                    }
                }

                SmogOffState::NO2Read => {
                    no2_sensor.update(&timer, &mut usb_serial);
                    thermometer.update(&mut wire, &mut delay, &mut usb_serial);
                    if timer_secs > NO2_READ_DURATION {
                        state = SmogOffState::WriteData;
                        timer_secs = 0;
                    }
                }

                SmogOffState::WriteData => {
                    match volume_mgr.device().num_bytes() {
                        Ok(bytes) => {
                            led_pin.set_high().unwrap();
                            let mut text_buffer: String<64> = String::new();

                            let mut volume =
                                match volume_mgr.open_volume(embedded_sdmmc::VolumeIdx(0)) {
                                    Ok(v) => v,
                                    Err(e) => {
                                        writeln!(&mut text_buffer, "volume err {:?}\n", e).unwrap();
                                        usb_serial.log(&text_buffer);
                                        continue;
                                    }
                                };

                            let mut dir = match volume.open_root_dir() {
                                Ok(v) => v,
                                Err(e) => {
                                    writeln!(&mut text_buffer, "root err {:?}\n", e).unwrap();
                                    usb_serial.log(&text_buffer);
                                    continue;
                                }
                            };

                            let mut file = match dir.open_file_in_dir(
                                "SMOGOFF.TXT",
                                embedded_sdmmc::Mode::ReadWriteCreateOrAppend,
                            ) {
                                Ok(v) => v,
                                Err(e) => {
                                    writeln!(&mut text_buffer, "file err {:?}\n", e).unwrap();
                                    usb_serial.log(&text_buffer);
                                    continue;
                                }
                            };

                            usb_serial.log("writing data\n");

                            {
                                let mut text_buffer: String<64> = String::new();
                                writeln!(
                                    &mut text_buffer,
                                    "pm 2.5: {} temp: {}\n",
                                    pm_sensor.current_values.pmc_std_2_5, thermometer.temperature
                                )
                                .unwrap();
                                //screen.print(&text_buffer);
                            }

                            let mut text_buffer: String<512> = String::new();
                            writeln!(
                                &mut text_buffer,
                                "{}, {}",
                                rtc.get_timestamp_str(),
                                pm_sensor.data_string(),
                            )
                            .unwrap();
                            let _ = file.write(text_buffer.as_bytes()).unwrap();

                            led_pin.set_low().unwrap();
                        }
                        Err(e) => {
                            //screen.print("no sdcard");
                            volume_mgr.device().mark_card_uninit();
                            let mut text_buffer: String<64> = String::new();
                            writeln!(&mut text_buffer, "card err {:?}\n", e).unwrap();
                            usb_serial.log(&text_buffer);
                        }
                    }

                    state = SmogOffState::WriteData;
                }

                SmogOffState::Idle => {
                    if sampling_secs > SAMPLING_PERIOD {
                        state = SmogOffState::PMStart;
                        timer_secs = 0;
                    }
                }
            };

            next_update_time += 1_000_000;
            timer_secs += 1;
            sampling_secs += 1;

            /*if state != last_state {
                let mut text_buffer: String<64> = String::new();
                writeln!(&mut text_buffer, "state is {:?}\n", state).unwrap();
                screen.print(&text_buffer);
                last_state = state;
            }*/

            // Scan for devices on the bus by attempting to read from them

            // let mut text_buffer: String<64> = String::new();

            // usb_serial.log("scanning i2c\n");
            // for i in 0..=127u8 {
            //     let mut readbuf: [u8; 1] = [0; 1];
            //     let result = i2c.read(i, &mut readbuf);
            //     if let Ok(d) = result {
            //         writeln!(&mut text_buffer, "found {}", i).unwrap();
            //         usb_serial.log(&text_buffer);
            //     }
            // }
        }

        usb_serial.usb_dev.poll(&mut [&mut usb_serial.serial]);
    }
}
