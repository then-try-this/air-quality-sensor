// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use core::fmt::Write;
use embedded_hal::i2c::I2c;
use heapless::String;

const DEVICE_ADDR: u8 = 0x68;

const REG_CTL_1: u8 = 0x00;
const REG_CTL_2: u8 = 0x01;
const REG_CTL_3: u8 = 0x02;
const REG_SEC: u8 = 0x03;

const CTL_3_BLF: u8 = 0x01 << 2;

fn bcd_decode(x: u8) -> u32 {
    ((((x & 0xf0) >> 4) * 10) + (x & 0x0f)) as u32
}

fn bcd_encode(x: u32) -> u8 {
    if x >= 100 {
        panic!("Tried to BCD encode value {} >= 100", x);
    }
    let lower = x % 10;
    let upper = x / 10;
    (lower | (upper << 4)) as u8
}

pub struct Pcf8523<I2C> {
    dev: I2C,
}

impl<I2C> Pcf8523<I2C>
where
    I2C: I2c,
{
    pub fn new(dev: I2C) -> Self {
        Pcf8523 { dev }
    }

    pub fn battery_ok(&mut self) -> Result<bool, I2C::Error> {
        let mut buf: [u8; 1] = [0];
        let _ = self.dev.write_read(DEVICE_ADDR, &[REG_CTL_3], &mut buf)?;
        Ok(buf[0] & CTL_3_BLF != 0)
    }

    pub fn get_time(&mut self) -> Result<[u32; 6], I2C::Error> {
        let mut buf: [u8; 7] = [0; 7];

        let _ = self.dev.write_read(DEVICE_ADDR, &[REG_SEC], &mut buf)?;

        let sec = bcd_decode(buf[0]) - 80; // ???
        let min = bcd_decode(buf[1]);
        let hour = bcd_decode(buf[2]);
        let day = bcd_decode(buf[3]);
        let mon = bcd_decode(buf[5]);
        let yr = bcd_decode(buf[6]);

        Ok([2000 + yr as u32, mon, day, hour, min, sec])
    }

    pub fn get_timestamp_str(&mut self) -> String<128> {
        let mut text_buffer: String<128> = String::new();
        if let Ok(time) = self.get_time() {
            write!(
                &mut text_buffer,
                "{}/{}/{} {}:{}:{}",
                time[0], time[1], time[2], time[3], time[4], time[5]
            )
            .unwrap();
        } else {
            writeln!(&mut text_buffer, "RTC read error",).unwrap();
        }
        text_buffer
    }

    /*    pub fn set_time(&mut self, time: chrono::DateTime<Utc>) {
            let sec = bcd_encode(time.second());
            let min = bcd_encode(time.minute());
            let hour = bcd_encode(time.hour());
            let day = bcd_encode(time.day());
            // chrono has Sunday == 7, PCF8523 has Sunday == 0 (like a sane person)
            let days_since_monday = time.weekday().number_from_monday();
            let dow = bcd_encode(if days_since_monday == 7 { 0 } else { days_since_monday });

            let mon = bcd_encode(time.month());
            let yr = bcd_encode(time.year() as u32 - 2000);
            let data = [sec, min, hour, day, dow, mon, yr];

        let _ = self.dev.write(0x03, &data);

        }
    */
}
