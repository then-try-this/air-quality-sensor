// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const PMS7003_ID: u16 = 0x424d;

/// The data on the PMS7003 is in bigendian format
fn swap_endian16(data: u16) -> u16 {
    data.rotate_right(8) | data.rotate_left(8)
}

/// The types of (documented) commands the device accepts
pub enum CommandType {
    Passive,
    ChangeMode,
    Sleep,
}

/// Data used by different commands
pub enum CommandData {
    Passive,
    Active,
    Sleep,
    Wakeup,
}

/// Converts commands to u8 value
fn command_type_to_u8(t: CommandType) -> u8 {
    match t {
        CommandType::Passive => 0xe2,
        CommandType::ChangeMode => 0xe1,
        CommandType::Sleep => 0xe4,
    }
}

/// Converts arguments to u8 value
fn command_data_to_u8(d: CommandData) -> u8 {
    match d {
        CommandData::Passive => 0x00,
        CommandData::Active => 0x01,
        CommandData::Sleep => 0x00,
        CommandData::Wakeup => 0x01,
    }
}

/// A command to tell the sensor to do something, includes a checksum.
#[derive(Clone, Copy)]
pub struct Command {
    pub cmd: u8,
    pub data_h: u8, // never used
    pub data_l: u8,
}

impl Command {
    /// Builds the command and calculates the correct checksum, ready
    /// for sending to the device.
    pub fn new(cmd: CommandType, data: CommandData) -> Self {
        Command {
            cmd: command_type_to_u8(cmd),
            data_h: 0,
            data_l: command_data_to_u8(data),
        }
    }

    pub fn as_bytes(&self) -> [u8; 7] {
        let bytes = [0x42, 0x4d, self.cmd, self.data_h, self.data_l];
        let mut total: u16 = 0;
        for i in 0..5 {
            total += bytes[i] as u16;
        }
        let v: [u8; 2] = total.to_be_bytes();
        [bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], v[0], v[1]]
    }
}

/// A packet is a single dump of the sensor data readings sent from
/// the device. Comprises standard (std) and environmentally corrected
/// PM per m3 readings as well as the number of particles at each
/// size.
#[derive(Clone, Copy)]
pub struct Packet {
    pub id: u16,
    pub frame_len: u16,

    pub pmc_std_1_0: u16,
    pub pmc_std_2_5: u16,
    pub pmc_std_10_0: u16,

    pub pmc_env_1_0: u16,
    pub pmc_env_2_5: u16,
    pub pmc_env_10_0: u16,

    pub np_0_3: u16,
    pub np_0_5: u16,
    pub np_1_0: u16,
    pub np_2_5: u16,
    pub np_5_0: u16,
    pub np_10_0: u16,

    pub reserved: u16,
    pub checksum: u16,
}

impl From<[u8; 32]> for Packet {
    fn from(item: [u8; 32]) -> Packet {
        unsafe { core::mem::transmute(item) }
    }
}

impl From<Packet> for [u8; 32] {
    fn from(item: Packet) -> [u8; 32] {
        unsafe { core::mem::transmute(item) }
    }
}

pub enum PacketResult {
    Ok(Packet),
    WrongID(u16),
    WrongChecksum(u16),
}

impl Packet {
    /// Create an empty, zerod packet
    pub fn new() -> Self {
        Packet {
            id: 0,
            frame_len: 0,
            pmc_std_1_0: 0,
            pmc_std_2_5: 0,
            pmc_std_10_0: 0,

            pmc_env_1_0: 0,
            pmc_env_2_5: 0,
            pmc_env_10_0: 0,

            np_0_3: 0,
            np_0_5: 0,
            np_1_0: 0,
            np_2_5: 0,
            np_5_0: 0,
            np_10_0: 0,

            reserved: 0,
            checksum: 0,
        }
    }

    /// Build from raw data transmitted from the device, and check the
    /// ID and checksum is correct
    pub fn from_bigendian(raw: [u8; 32]) -> PacketResult {
        let mut swapped = [0; 32];
        // swap bytes to convert to little endian
        for i in 0..16 {
            swapped[i * 2] = raw[i * 2 + 1];
            swapped[i * 2 + 1] = raw[i * 2];
        }

        // convert into a packet struct
        let p: Packet = swapped.into();

        // check what we have
        if p.id != PMS7003_ID {
            PacketResult::WrongID(p.id)
        } else if !p.check() {
            PacketResult::WrongChecksum(p.checksum)
        } else {
            PacketResult::Ok(p)
        }
    }

    pub fn check(&self) -> bool {
        let mut checksum: u16 = 0;
        let bytes: [u8; 32] = (*self).into();
        bytes.iter().take(30).for_each(|d| {
            checksum += *d as u16;
        });
        checksum == self.checksum
    }
}

// We need to be careful to process data from the sensor as it's not terribly
// well documented (not sure about the messages sent back after commands) and
// reads can happen in the middle of messages.
//
// 1. Take arbitrary data of arbitrary lengths
// 2. When one starts with the id bytes start the stream (state=concat)
// 3. Concatenate all following data into the stream until:
// 3a. New id bytes are found or
// 3b. 32 bytes is reached
// 4. Then copy the stream to msg and flag ready for reading

// todo: check the checksum for data messages

/*
const PMS7003_STREAM_SIZE: usize = 32;
const PMS7003_STREAM_MAX_SIZE: usize = 64;

fn id_check(data: &[u8; PMS7003_STREAM_MAX_SIZE], size: usize) -> bool {
    size > 2 && data[0] != (PMS7003_ID & 0x00ff) as u8 && data[1] == (PMS7003_ID >> 8) as u8
}

struct PMS7003Stream {
    msg_ready: bool,
    write_pos: usize,
    stream: [u8; PMS7003_STREAM_SIZE],
    msg_size: usize,
    msg: PMS7003Packet,
}

impl PMS7003Stream {
    fn new() -> Self {
        PMS7003Stream {
            msg_ready: false,
            write_pos: 0,
            stream: [0; PMS7003_STREAM_SIZE],
            msg_size: 0,
            msg: PMS7003Packet::new(),
        }
    }

    fn update(&mut self, data: &[u8; PMS7003_STREAM_MAX_SIZE], size: usize) {
        if size > PMS7003_STREAM_SIZE {
            return;
        }

        // is this the start of a new message?
        if id_check(data, size) {
            // if we have a message in the stream
            if self.write_pos > 0 {
                // copy it to the msg area and set msg size
                self.msg = unsafe { core::mem::transmute(self.stream) };
                self.msg_size = self.write_pos;
                self.msg_ready = true;
                // reset the stream
                self.write_pos = 0;
            }

            // add to the stream
            for i in 0..size {
                self.stream[i + self.write_pos] = data[i];
            }
            self.write_pos += size;
        } else {
            // only add this (intermediate message) if we are already writing
            // otherwise it's junk
            if self.write_pos > 0 {
                // add to the stream
                // add to the stream
                for i in 0..size {
                    self.stream[i + self.write_pos] = data[i];
                }
                self.write_pos += size;
            }
        }

        if self.write_pos >= PMS7003_STREAM_SIZE {
            // max size reached
            // copy it to the msg area and set msg size
            self.msg = unsafe { core::mem::transmute(self.stream) };
            self.msg_ready = true;
            self.msg_size = self.write_pos;
            // reset the stream
            self.write_pos = 0;
        }
    }
}

*/
