// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rp2040_hal::{
    gpio::{FunctionUart, Pin, PinId, PullDown},
    uart::*,
};

use usbd_serial::embedded_io::ReadReady;

use core::fmt::Write;
use heapless::String;
use pac::RESETS;
use rp2040_hal::fugit::Rate;
use rp2040_hal::fugit::RateExtU32;

use sensors::pms7003::*;
use USBSerial;

pub struct PMSensor<D: UartDevice, TX: PinId, RX: PinId>
where
    TX: ValidPinIdTx<D>,
    RX: ValidPinIdRx<D>,
{
    uart: UartPeripheral<
        Enabled,
        D,
        (
            Pin<TX, FunctionUart, PullDown>,
            Pin<RX, FunctionUart, PullDown>,
        ),
    >,
    pub current_values: Packet,
}

impl<D: UartDevice, TX: PinId, RX: PinId> PMSensor<D, TX, RX>
where
    TX: ValidPinIdTx<D>,
    RX: ValidPinIdRx<D>,
{
    pub fn new(
        pins: (
            Pin<TX, FunctionUart, PullDown>,
            Pin<RX, FunctionUart, PullDown>,
        ),
        device: D,
        resets: &mut RESETS,
        freq: Rate<u32, 1, 1>,
    ) -> Self {
        // Need to perform clock init before using UART or it will freeze.
        PMSensor {
            // todo: allow unitialised and remove unwrap
            uart: UartPeripheral::new(device, pins, resets)
                .enable(
                    UartConfig::new(9600_u32.Hz(), DataBits::Eight, None, StopBits::One),
                    freq,
                )
                .unwrap(),
            current_values: Packet::new(),
        }
    }

    pub fn data_string(&self) -> String<128> {
        let mut text_buffer: String<128> = String::new();
        write!(
            &mut text_buffer,
            "PM, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}",
            self.current_values.pmc_std_1_0,
            self.current_values.pmc_std_2_5,
            self.current_values.pmc_std_10_0,
            self.current_values.pmc_env_1_0,
            self.current_values.pmc_env_2_5,
            self.current_values.pmc_env_10_0,
            self.current_values.np_0_3,
            self.current_values.np_0_5,
            self.current_values.np_1_0,
            self.current_values.np_2_5,
            self.current_values.np_5_0,
            self.current_values.np_10_0
        )
        .unwrap();
        text_buffer
    }

    pub fn passive(&mut self) {
        let data: [u8; 7] = Command::new(CommandType::Passive, CommandData::Passive).as_bytes();
        //let data: [u8; 7] = [0x4d, 0x42, 0xe4, 0x00, 0x00, 0x73, 0x01];
        self.uart.write_full_blocking(&data);
    }

    pub fn sleep(&mut self, usb_serial: &mut USBSerial) {
        let data: [u8; 7] = Command::new(CommandType::Sleep, CommandData::Sleep).as_bytes();
        //let data: [u8; 7] = [0x42, 0x4d, 0xe4, 0x00, 0x00, 0x01, 0x73];

        let mut text_buffer: String<64> = String::new();
        writeln!(&mut text_buffer, "{:?}", data).unwrap();
        usb_serial.log(&text_buffer);

        self.uart.write_full_blocking(&data);
    }

    pub fn wake(&mut self) {
        let data: [u8; 7] = Command::new(CommandType::Sleep, CommandData::Wakeup).as_bytes();
        //let data: [u8; 7] = [0x4d, 0x42, 0xe4, 0x00, 0x00, 0x73, 0x01];
        self.uart.write_full_blocking(&data);
    }

    pub fn update(&mut self, usb_serial: &mut USBSerial) {
        let mut finished = false;
        while !finished {
            if let Ok(ready) = self.uart.read_ready() {
                if ready {
                    usb_serial.log("plantower says: \n");

                    let mut uart_buffer: [u8; 32] = [0; 32];
                    let _ = self.uart.read_full_blocking(&mut uart_buffer);
                    let result = Packet::from_bigendian(uart_buffer);

                    if let Ok(ready) = self.uart.read_ready() {
                        if ready {
                            usb_serial.log("more waiting...\n");
                        } else {
                            finished = true;
                        }
                    } else {
                        finished = true;
                    }

                    let mut text_buffer: String<64> = String::new();

                    match result {
                        PacketResult::Ok(p) => {
                            writeln!(&mut text_buffer, "ok -> pmc_std_2_5: {}", p.pmc_std_2_5)
                                .unwrap();
                            usb_serial.log(&text_buffer);
                            self.current_values = p;
                        }
                        PacketResult::WrongID(e) => {
                            writeln!(&mut text_buffer, "wrong id: {}", e).unwrap();
                            usb_serial.log(&text_buffer);
                        }
                        PacketResult::WrongChecksum(e) => {
                            writeln!(&mut text_buffer, "wrong checksum: {}", e).unwrap();
                            usb_serial.log(&text_buffer);
                        }
                    }
                }
            }
        }
    }
}
