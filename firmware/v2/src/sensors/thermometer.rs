// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use core::fmt::{Debug, Write};
use cortex_m::delay::Delay;
use heapless::String;
use onewire::*;

use USBSerial;

pub struct Thermometer {
    sensor: Option<DS18B20>,
    pub temperature: f32,
}

impl Thermometer {
    pub fn new() -> Self {
        Thermometer {
            sensor: None,
            temperature: 99.,
        }
    }

    pub fn split_temp(temperature: u16) -> (i16, i16) {
        if temperature < 0x8000 {
            (temperature as i16 >> 4, (temperature as i16 & 0xF) * 625)
        } else {
            let abs = -(temperature as i16);
            (-(abs >> 4), -625 * (abs & 0xF))
        }
    }

    pub fn convert_to_celsius(temperature: u16) -> f32 {
        let s = Self::split_temp(temperature);
        s.0 as f32 + s.1 as f32 / 10000.0
    }

    pub fn update<E: Debug>(
        &mut self,
        bus: &mut OneWire<E>,
        delay: &mut Delay,
        usb_serial: &mut USBSerial,
    ) {
        let mut text_buffer: String<64> = String::new();
        match &self.sensor {
            Some(sensor) => {
                // read temperature
                usb_serial.log("trying to read temp\n");

                // request sensor to measure temperature
                let resolution = sensor.measure_temperature(bus, delay).unwrap();

                // wait for compeltion, depends on resolution
                delay.delay_ms(resolution.time_ms().into());

                match sensor.read_temperature(bus, delay) {
                    Ok(t) => {
                        self.temperature = Self::convert_to_celsius(t);
                        writeln!(text_buffer, "temp is {} ({})", self.temperature, t).unwrap();
                        usb_serial.log(&text_buffer);
                    }
                    Err(e) => {
                        writeln!(text_buffer, "error reading temp {:?}", e).unwrap();
                        usb_serial.log(&text_buffer);
                    }
                }
            }
            None => {
                // search for devices
                usb_serial.log("searching for thermometer\n");
                let mut search = DeviceSearch::new();
                while let Some(device) = bus.search_next(&mut search, delay).unwrap() {
                    usb_serial.log("thermometer setup started\n");
                    match device.address[0] {
                        ds18b20::FAMILY_CODE => {
                            if let Ok(mut sensor) = DS18B20::new::<u32>(device) {
                                usb_serial.log("thermometer setup success\n");
                                self.sensor = Some(sensor);
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    }
}
