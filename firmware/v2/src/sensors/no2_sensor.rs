// Smogoff Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use embedded_ads111x as ads111x;
use embedded_ads111x::{ADS111x, ADS111xConfig};
//use embedded_hal::i2c::{ Operation::Write as I2CWrite, WriteRead };
use core::fmt::Write;
use heapless::String;
use rp2040_hal::Timer;
use USBSerial;

// sensor A
const WE_EL_ZERO_MV: f32 = 218.0;
const AUX_EL_ZERO_MV: f32 = 250.0;
const SENSITIVITY: f32 = 0.271;
const WE_ZERO_MV: f32 = 208.0;
const AUX_ZERO_MV: f32 = 241.0;

const PPB_TO_UGM3: f32 = 1.9125;

#[derive(Debug)]
enum NO2SensorState {
    Good,
    NoAddress,
    NoConfig,
}

pub struct NO2Sensor<I2C> {
    adc: Option<ADS111x<I2C>>,
    state: NO2SensorState,
    temp_values: [f32; 9],
    correction_values_alg1: [f32; 9],
    correction_values_alg3: [f32; 9],
}

impl<I2C> NO2Sensor<I2C>
where
    I2C: embedded_hal::i2c::I2c, //    I2C:WriteRead<Error = E>,
                                 //    I2C:I2CWrite<Error = E>, E: core::fmt::Debug,
{
    pub fn new(i2c: I2C) -> Self {
        let config = ADS111xConfig::default()
            .mux(ads111x::InputMultiplexer::AIN0AIN1)
            .mux(ads111x::InputMultiplexer::AIN1AIN3)
            .dr(ads111x::DataRate::SPS8)
            .pga(ads111x::ProgramableGainAmplifier::V6_144);

        let mut state = NO2SensorState::Good;

        let adc = if let Ok(mut adc) = ADS111x::new(i2c, 0x48u8, config) {
            if let Err(e) = adc.write_config(None) {
                state = NO2SensorState::NoConfig;
                None
            } else {
                Some(adc)
            }
        } else {
            state = NO2SensorState::NoAddress;
            None
        };

        NO2Sensor {
            adc,
            state,
            temp_values: [-30.0, -20.0, -10.0, 0.0, 10.0, 20.0, 30.0, 40.0, 50.0],
            // CORRECTION VALUES - MAKE SURE TO ADJUST "ppb ="
            // calculation below accordingly using algorithms in
            // Alphasense application note AAN-803-05 (note that
            // Algorithms 1 and 3 are suggested for NO2)
            correction_values_alg1: [1.3, 1.3, 1.3, 1.3, 1.0, 0.6, 0.4, 0.2, -1.5],
            correction_values_alg3: [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.4, -0.1, -4.0],
        }
    }

    pub fn update(&mut self, timer: &Timer, usb_serial: &mut USBSerial) -> f32 {
        if let Some(ref mut adc) = &mut self.adc {
            let mut sum_we: f32 = 0.0;
            let mut sum_aux: f32 = 0.0;
            let mut count: u64 = 1;
            let mut text_buffer: String<64> = String::new();

            sum_we += match adc.read_single_voltage(Some(ads111x::InputMultiplexer::AIN0AIN1)) {
                Ok(v) => f32::from(v) * 6.144f32 / f32::from(i16::MAX),
                Err(e) => {
                    writeln!(&mut text_buffer, "err {:?}", e).unwrap();
                    usb_serial.log(&text_buffer);
                    0.
                }
            };

            sum_aux += match adc.read_single_voltage(Some(ads111x::InputMultiplexer::AIN2AIN3)) {
                Ok(v) => f32::from(v) * 6.144f32 / f32::from(i16::MAX),
                Err(e) => {
                    //writeln!(&mut text_buffer, "err {:?}", e).unwrap();
                    //usb_serial.log(&text_buffer);
                    0.
                }
            };

            let we_mv = sum_we / (count as f32);
            let aux_mv = sum_aux / (count as f32);

            // we use uncorrected values and post-process them based on
            // recorded temperature
            let ppb = ((we_mv - WE_EL_ZERO_MV) - (aux_mv - AUX_EL_ZERO_MV)) / SENSITIVITY; // USE THIS FOR UNCORRECTED VALUES
                                                                                           // float ppb = ((we_mv-WE_EL_ZERO_MV)-(nt*(aux_mv-AUX_EL_ZERO_MV)))/SENSITIVITY; // USE THIS IF USING ALGORITHM 1
                                                                                           // float ppb = ((we_mv-WE_EL_ZERO_MV)-((WE_ZERO_MV-WE_EL_ZERO_MV)-(AUX_ZERO_MV-AUX_EL_ZERO_MV))-(kt*(aux_mv-AUX_EL_ZERO_MV)))/SENSITIVITY; // USE THIS IF USING ALGORITHM 3

            writeln!(
                &mut text_buffer,
                "{:?} no2: we {}mV aux {}mV",
                self.state, we_mv, aux_mv
            )
            .unwrap();
            usb_serial.log(&text_buffer);

            ppb * PPB_TO_UGM3
        } else {
            //self.init(usb_serial);
            usb_serial.log("no ads111x");
            return 0.;
        }
    }
}
