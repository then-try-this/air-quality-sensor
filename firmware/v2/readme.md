# Smogoff Rust firmware version for the Raspberry Pi Pico

* USB uart for logging build: [x] tested: [x] 
* Plantower PM7003 Particulate matter sensor (uart) build: [x] tested: [x] 
* Alphasense NO2 sensor (via ads111x 16bit analogue to digital i2c device) build: [x] test: [x]  
* Thermometer (one-wire) build: [x] tested: [x] 
* Paperwhite screen (spi0) build: [x] tested: [x] 
* SDcard for logging data (spi0) build: [x] tested: [x] 
* RTC for logging real times (i2c) build: [x] tested: [x] 

Building:

    cargo build

Running on the rp2040 (after pressing the button while plugging in):

    cargo run --bin smogoff
    
# Notes

I found some useful bits of code here:
https://github.com/rp-rs/rp-hal-boards/blob/main/boards/rp-pico/examples
(found way too late)

