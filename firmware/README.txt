### firmware ###

This folder contains the firmware (Arduino sketches) for the SmogOff sensors

--------------------------------------------------------------------------------

Each individual SmogOff unit has different code because of the different factory 
calibration values of the Alphasense NO2 sensors.

# "smogoff_alphasense_ADC_A

This is for SmogOff unit A

# "smogoff_alphasense_ADC_B

This is for SmogOff unit B

# "smogoff_alphasense_ADC_C

This is for SmogOff unit C

--------------------------------------------------------------------------------

# "data_visualisation"

This sketch is uploaded onto the Arduino Nano Every.
This is the code for the real-time data visualisations.

--------------------------------------------------------------------------------

SmogOff units A and B have a different data logging shield to unit C.
A and B's data logging shield has a PCF8523 real-time clock
C's data logging shield has a DS1307 realtime clock

# "pcf8523_setRTC"

Use this code to set the realtime clock for units A and B

# "ds1307_setRTC"

Use this code to set the realtime clock for unit C
