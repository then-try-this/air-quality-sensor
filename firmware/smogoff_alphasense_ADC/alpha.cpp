#include "alpha.h"
Adafruit_ADS1115 ads1115;
#include "InterpolationLib.h"

const int numValues = 9;

// TEMP VALUES
double TempValues[9] = {  -30, -20, -10, 0, 10, 20, 30, 40, 50 }; 

// CORRECTION VALUES - MAKE SURE TO ADJUST "ppb =" calculation below accordingly using algorithms in Alphasense application note AAN-803-05 (note that Algorithms 1 and 3 are suggested for NO2)
// double CorrectionValues[9] = { 1.3, 1.3, 1.3, 1.3, 1.0, 0.6, 0.4, 0.2, -1.5 }; // nt (Algorithm 1)
double CorrectionValues[9] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.4, -0.1, -4 };  //k't (Algorithm 3)

// takes mean mV over each sampling period
void alpha_diff_mv(float sample_period, float &we_mv, float &aux_mv) {
  unsigned long elapsed_time, n_samples = 0;
  long sum_we = 0;
  long sum_aux = 0;
  elapsed_time = millis() + sample_period * 1000;
  do {
          sum_we += ads1115.readADC_Differential_0_1()*0.1875;
          sum_aux += ads1115.readADC_Differential_2_3()*0.1875;
    delay(1);
    n_samples++;
  } while (millis() < elapsed_time);
  we_mv = float (sum_we) / float(n_samples);
  aux_mv = float (sum_aux) / float(n_samples);
}

float alpha_read_ugm3(float sample_period, float &we_mv, float &aux_mv, float TempC) {
  alpha_diff_mv(sample_period, we_mv, aux_mv);
  // float kt =  Interpolation::Linear(TempValues, CorrectionValues, numValues, TempC, false); // k't is correction factor for Algorithm #3 in the Alphasense application note
  // float nt =  Interpolation::Linear(TempValues, CorrectionValues, numValues, TempC, false); // nt is correction factor for Algorithm #1 in the Alphasense application note
  // Serial.print("kt:");
  // Serial.println(kt); 
  float ppb = ((we_mv-WE_EL_ZERO_MV)-(aux_mv-AUX_EL_ZERO_MV))/SENSITIVITY;// USE THIS FOR UNCORRECTED VALUES
  // float ppb = ((we_mv-WE_EL_ZERO_MV)-(nt*(aux_mv-AUX_EL_ZERO_MV)))/SENSITIVITY; // USE THIS IF USING ALGORITHM 1
  // float ppb = ((we_mv-WE_EL_ZERO_MV)-((WE_ZERO_MV-WE_EL_ZERO_MV)-(AUX_ZERO_MV-AUX_EL_ZERO_MV))-(kt*(aux_mv-AUX_EL_ZERO_MV)))/SENSITIVITY; // USE THIS IF USING ALGORITHM 3
  return ppb*ppb2ugm3;
}
