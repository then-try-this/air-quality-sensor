// Sonic Kayaks Copyright (C) 2020 FoAM Kernow
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include "pms7003.h"
#include "RTClib.h"
#include "alpha.h"
#include <OneWire.h>
#include <DallasTemperature.h>
 
const long PM_READ_DURATION = 10000;
const long PM_WARMUP_DURATION = 3000;
const long PM_WARMDOWN_DURATION = 1000;
const long PM_MAX_ERRORS = 50; // 50 read fails @ 100ms per attempt
const long SAMPLING_PERIOD = 60000;
const long NO2_SAMPLE_PERIOD = 1000;


#define STATE_PM_START 0
#define STATE_PM_WARMUP 1
#define STATE_PM_READ 2
#define STATE_PM_STOP 3
#define STATE_PM_WARMDOWN 4
#define STATE_NO2_READ 5
#define STATE_IDLE 6


#define ONE_WIRE_BUS 7


// globals
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
SoftwareSerial pmSerial(4, 5); // TX, RX
pms7003_stream pm_stream;
long timer=0;
unsigned char state = 0;

float TempC;
// long temp;
long b;  
short pm25;
unsigned char error_code = 0;

#define ERROR_SDCARD 1
#define ERROR_PM_NODATA 2
#define ERROR_RTC_INIT 3
#define ERROR_NOTEMP 4

long start_time = 0;

char zeroed = 0;

// box num set in alpha.h

#ifdef SMOGOFF_A
RTC_PCF8523 rtc; // adafruit shield
#endif

#ifdef SMOGOFF_B
RTC_PCF8523 rtc; // adafruit shield
#endif

#ifdef SMOGOFF_C
RTC_DS3231 rtc; // el cheapo shield
#endif

void error_warning() {
  for (unsigned int i=0; i<1; i++) {
    digitalWrite(3,HIGH); 
    delay(100); 
    digitalWrite(3,LOW);
    delay(100);
  }
}

void display_ok() {
  for (unsigned int i=0; i<10; i++) {
    digitalWrite(3,HIGH); 
    delay(50); 
    digitalWrite(3,LOW);
    delay(50);
  }
}


void sdlog(int type, float a, float b, float c, float d) {  
  // see if the card is present and can be initialized:
  if (!SD.begin(10)) {
    error_warning();
    Serial.println("SD card not initialised!");    
    error_code=ERROR_SDCARD;
  }
   
  DateTime now = rtc.now();
  
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (dataFile) {
    if (error_code==ERROR_SDCARD) error_code=0; 
    dataFile.print(now.year(), DEC);
    dataFile.print('/');
    dataFile.print(now.month(), DEC);
    dataFile.print('/');
    dataFile.print(now.day(), DEC);
    dataFile.print(' ');
    dataFile.print(now.hour(), DEC);
    dataFile.print(':');
    dataFile.print(now.minute(), DEC);
    dataFile.print(':');
    dataFile.print(now.second(), DEC);
    dataFile.print(", "); 
    dataFile.print(millis());
    dataFile.print(", ");
    switch (type) {
      case 0: dataFile.print("pm, "); break;
      case 1: dataFile.print("no2, "); break;
      default: dataFile.print("system started, "); break;
    }
    dataFile.print(a,5);
    dataFile.print(", ");    
    dataFile.print(b,5);
    dataFile.print(", ");    
    dataFile.print(c,5);
    dataFile.print(", ");    
    dataFile.println(d,5);
    dataFile.close();

  } else {
    Serial.println("error opening datalog.txt");
    error_warning();
  }

    Serial.print(now.year(), DEC);
     Serial.print('/');
     Serial.print(now.month(), DEC);
     Serial.print('/');
     Serial.print(now.day(), DEC);
     Serial.print(' ');
     Serial.print(now.hour(), DEC);
     Serial.print(':');
     Serial.print(now.minute(), DEC);
     Serial.print(':');
     Serial.print(now.second(), DEC);
     Serial.print(" : ");     
  
    if (type==0) Serial.print("pm, ");
    else Serial.print("no2, ");    
    Serial.print(a,5);
    Serial.print(", ");    
    Serial.print(b,5);
    Serial.print(", ");    
    Serial.print(c,5);
    Serial.print(", ");    
    Serial.println(d,5);
  
}

char have_pm_data=0;

void check_pm() {
  unsigned long len=pmSerial.available();
  if (len > 0) {
    unsigned char buf[64]; 
    pmSerial.readBytes((char *)buf,len);
    pms7003_stream_update(&pm_stream,buf,len); 
    if (pm_stream.msg_ready==1 && pm_stream.msg_size==32) {      
      pm_stream.msg_ready=0;  
      if (pms7003_check_packet(&pm_stream.msg)) {
        have_pm_data=1;
        pm25 = swap_endian(pm_stream.msg.pmc_std_2_5);

        sdlog(0,swap_endian(pm_stream.msg.pmc_std_1_0),
                swap_endian(pm_stream.msg.pmc_std_2_5),
                swap_endian(pm_stream.msg.pmc_std_10_0),0);
      }
    } 
  }
}

// will block for ~one second
void read_no2() {
  float we_mv;
  float aux_mv;
  float ugm3 = alpha_read_ugm3((NO2_SAMPLE_PERIOD/1000.0f),we_mv,aux_mv,TempC);  
  b = ugm3;
 // temp = 0; 
  sdlog(1,TempC,we_mv,aux_mv,ugm3);
  //Serial.println(ads1115.readADC_Differential_0_1());
  //Serial.println(ads1115.readADC_Differential_2_3());
}

void setup() {                
 
  Serial.begin(9600);
   // init the i2c output
  pinMode(3,OUTPUT);
  Wire.begin(9);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
  ads1115.begin();
  ads1115.setGain(GAIN_TWOTHIRDS); 
  sensors.begin();
  sensors.setResolution(12);
  int numberOfDevices = sensors.getDeviceCount();
  if (numberOfDevices < 1) {
    Serial.println("Can't find Temp sensor");
    while (1) {delay(1000);};
  }
 // init the air particulate matter sensor
  pms7003_stream_init(&pm_stream);
  pmSerial.begin(9600);
  // send it to sleep until it's needed
  pms7003_command cmd;
  pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_sleep);
  pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
    
  // test the sdcard
  SD.begin(10);
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (!dataFile) {
    error_warning();
  } 
  dataFile.close();

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    error_code = ERROR_RTC_INIT;
    error_warning();
    while (1) {delay(1000);};
  } 
  
  sdlog(3,0,0,0,0);
  
  state = STATE_NO2_READ;
  timer = millis();

  display_ok();
}


void loop() {  

  switch (state) {
    case STATE_PM_START: {
      Serial.println("starting pm");
      pms7003_command cmd;
      pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_wakeup);
      pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
      state=STATE_PM_WARMUP;
      timer=millis();
      start_time = millis();
    } break;

    case STATE_PM_WARMUP: {
      if (millis()-timer>PM_WARMUP_DURATION) {
        state=STATE_PM_READ;
        timer=millis();
        have_pm_data=0;
      } 
    } break;
    
    case STATE_PM_READ: {
      check_pm();
      if (millis()-timer>PM_READ_DURATION) {
        state=STATE_PM_STOP;
      } 
    } break;

    case STATE_PM_STOP: {
      Serial.println(error_code);
      if (have_pm_data==0) { 
        error_code=ERROR_PM_NODATA;
      } else {
        if (error_code==ERROR_PM_NODATA) {            
          error_code=0;
        }
      }
      
      Serial.println("stopping pm");
      pms7003_command cmd;
      pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_sleep);
      pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
      state=STATE_PM_WARMDOWN;
      timer=millis();      
    } break;

    case STATE_PM_WARMDOWN: {
      if (millis()-timer>PM_WARMDOWN_DURATION) {
        state=STATE_NO2_READ;
      } 
    } break;


    case STATE_NO2_READ: {
      sensors.requestTemperatures();
      TempC = sensors.getTempCByIndex(0);
      read_no2();
      state=STATE_IDLE;
      
    } break;

    case STATE_IDLE: {
      if (millis()-start_time>SAMPLING_PERIOD) {
        state=STATE_PM_START;
        }
    } break;

    default: break;
  }
  
  delay(100);
}

void requestEvent() {
  Wire.write(99);
  Wire.write((unsigned char *)&TempC,sizeof(float));
  Wire.write((unsigned char *)&b,sizeof(long));  
  Wire.write((unsigned char *)&pm25,sizeof(short));
  Wire.write(error_code);
}
