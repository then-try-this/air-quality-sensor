#include <Adafruit_ADS1X15.h>

#include <ADS1115-Driver.h>

#ifndef ALPHA_h
#define ALPHA_h

#include "Arduino.h"
//#include <Adafruit_ADS1X15.h>

extern Adafruit_ADS1115 ads1115;

const int alpha_WEp_pin=A0;
const int alpha_WEn_pin=A1;
const int alpha_Auxp_pin=A2;
const int alpha_Auxn_pin=A3;

const float alpha_pVcc = 4.99;

#define SMOGOFF_B

#ifdef SMOGOFF_A
const float WE_EL_ZERO_MV = 218;  
const float AUX_EL_ZERO_MV = 250;  
const float SENSITIVITY = 0.271;
const float WE_ZERO_MV = 208;
const float AUX_ZERO_MV = 241;
#endif

#ifdef SMOGOFF_B
const float WE_EL_ZERO_MV = 226;  
const float AUX_EL_ZERO_MV = 227;  
const float SENSITIVITY = 0.318;
const float WE_ZERO_MV = 230;
const float AUX_ZERO_MV = 229;
#endif

#ifdef SMOGOFF_C
const float WE_EL_ZERO_MV = 239;  
const float AUX_EL_ZERO_MV = 240;  
const float SENSITIVITY = 0.320;
const float WE_ZERO_MV = 232;
const float AUX_ZERO_MV = 238;
#endif


const float ppb2ugm3 = 1.9125;

float alpha_read_ugm3(float sample_period, float &we_mv, float &aux_mv, float TempC);

#endif
