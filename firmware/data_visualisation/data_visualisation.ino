#include <Wire.h>
#include <Adafruit_GFX.h>
#include <math.h>
#include "Adafruit_ThinkInk.h"
#include "Adafruit_EPD.h"
#include <SdFat.h>                // SD card & FAT filesystem library
#include <Adafruit_SPIFlash.h>    // SPI / QSPI flash library
#include <Adafruit_ImageReader_EPD.h> // Image-reading functions

#define USE_SD_CARD

#define EPD_DC      2 
#define EPD_CS      3
#define SRAM_CS     4
#define EPD_RESET   5 // can set to -1 and share with microcontroller Reset!
#define EPD_BUSY   -1 // can set to -1 to not use a pin (will wait a fixed delay)
#define SD_CS       8 // SD card chip select

#define COLOR0 EPD_WHITE
#define COLOR1 EPD_BLACK
#define COLOR2 EPD_LIGHT
#define COLOR3 EPD_DARK

#define ERROR_SDCARD 1
#define ERROR_PM_NODATA 2
#define ERROR_RTC_INIT 3
#define ERROR_NOTEMP 4
#define ERROR_NO_SENSOR_ARD 99

// 2.9" Grayscale Featherwing or Breakout:
//ThinkInk_290_Grayscale4_T5 display(EPD_DC, EPD_RESET, EPD_CS, SRAM_CS, EPD_BUSY);
ThinkInk_290_Mono_M06 display(EPD_DC, EPD_RESET, EPD_CS, SRAM_CS, EPD_BUSY);

SdFat                    SD;         // SD card filesystem
Adafruit_ImageReader_EPD reader(SD); // Image-reader object, pass in SD filesys


Adafruit_Image_EPD   img;        // An image loaded into RAM
int32_t              width  = 0, // BMP image dimensions
                    height = 0;



struct sensor_data_t {
  unsigned char id;
  float TempC;
  long ugm3;
  short pm25;
  unsigned char error_code;
};     

sensor_data_t sensor_data;
const uint8_t i2c_address = 9;

uint16_t read_sensor_data()
{
    uint16_t length=sizeof(sensor_data);
    uint16_t bytes_received = 0;
    uint16_t bytes_requested = min(length,16);

/*    Wire.beginTransmission(i2c_address);
    uint16_t location = 0;
    Wire.send((uint8_t)(location >> 8));
    Wire.send((uint8_t)(location & 0xff));
    Wire.endTransmission();
*/
    Wire.requestFrom(i2c_address,bytes_requested);
    uint16_t remaining = bytes_requested;
    uint8_t* next = (uint8_t*)&sensor_data;

    while (Wire.available() && remaining--)
    {
        *next++ = Wire.read();
        ++bytes_received;
    }

    return bytes_received;
}


void setup() {
  //ImageReturnCode stat; // Status from image-reading functions
  Serial.begin(9600);
  Serial.println("setup");
 if(!SD.begin(SD_CS, SD_SCK_MHZ(10))) { // Breakouts require 10 MHz limit due to longer wires
    Serial.println(F("SD begin() failed"));
    //for(;;); // Fatal error, do not continue
  }
 display.begin(THINKINK_MONO);
 Wire.begin();
}

void draw_image(uint8_t t) {

  switch (t) {
    case 0:
      reader.drawBMP((char *)"/smileface.bmp", display, 90, 0);
      break;
    case 1: 
      reader.drawBMP((char *)"/neutralface.bmp", display,90, 0);
      break;
    case 2:
      reader.drawBMP((char *)"/sadface.bmp", display, 90, 0);
      break;
    case 3:
      reader.drawBMP((char *)"/thentrythis.bmp", display, 0, 0);
      break;
    default:      
      display.drawCircle(0, 0, 10, EPD_BLACK);  
      break;
  }
 
}

// function to calculate offset from decimal point for printing purposes!

int calc_offset(float value) {
  int char_offset = log10(abs(value));
  if (value < 0) {
    char_offset=char_offset+1; // this deals with negative character e.g. for no2 value
  }
  if (value < 1 && value >= 0) {
    char_offset = char_offset ; // this deals with 0 values (previously was ignoring the character before the decimal point in 0.00
  }
  return char_offset*12;
}


void loop() {  
    float no2 = 0;
    float pm = 0;
    float temp = 0;
    if(read_sensor_data()==sizeof(sensor_data) && 
       sensor_data.id==99 && 
       sensor_data.error_code==0) {  
      Serial.print(sensor_data.TempC);
      Serial.print(", ");
      Serial.print(sensor_data.ugm3);
      Serial.print(", ");
      Serial.println(sensor_data.pm25);

      no2 = max(0, sensor_data.ugm3);
      pm = sensor_data.pm25;
      temp = sensor_data.TempC;

      float no2_low = 7.0;
      float no2_high = 12.0;
      float pm_low = 3.0;
      float pm_high = 6.0;
      uint8_t face = 0;

    
      if (no2>no2_high) {
        face=2;
      } else {
        if (no2>no2_low) face=1;
      }    

      if (pm>pm_high) {
        face=2;
      } else {
        if (pm>pm_low && face!=2) face=1;
      }    
      display.clearBuffer();
      draw_image(face);
      display.display();
  
      delay(15*1000);

      display.clearBuffer();
      display.setTextSize(2);
      display.setTextColor(EPD_BLACK);
      display.setCursor(10, 40);
      display.print("NO2   = ");
      display.setCursor(140 - calc_offset(no2), 40);
      Serial.println(calc_offset(no2)); 
      display.print(no2);
      display.print(" ug/m3");
      display.setCursor(10, 60);
      display.print("PM2.5 = ");
      display.setCursor(140 - calc_offset(pm), 60);
      Serial.println(calc_offset(pm));
      display.print(pm);
      display.print(" ug/m3");
      display.setCursor(10, 80);
      display.print("Temp  = ");
      display.setCursor(140 - calc_offset(temp), 80);
      Serial.println(calc_offset(temp));
      display.print(temp);
      display.print(" C");
      display.display(); 

      delay(15*1000);

      display.clearBuffer();
      draw_image(3);
      display.setTextSize(3);
      display.setTextColor(EPD_BLACK);
      display.setCursor(152, 10);
      display.print("SmogOff");
      display.setTextSize(2);
      display.setCursor(135, 53);
      display.print("Air Pollution");
      display.setCursor(175, 73);
      display.print("Sensor");
      display.setTextSize(2);
      display.setCursor(115, 107);
      display.print("thentrythis.org");

      display.display();
 
      delay(10*1000);
    } else {
      if (sensor_data.error_code==0) {
        errorScreen(ERROR_NO_SENSOR_ARD);
        delay(5*1000);
      } else {
        errorScreen(sensor_data.error_code);
        delay(5*1000);
      }
    }
}

void errorScreen(unsigned char error_code){
  display.clearBuffer();
  display.setTextSize(2);
  display.setTextColor(EPD_BLACK);
  display.setCursor(10, 10);
  display.print("We have an error...");
  display.setCursor(10, 40);

  if (error_code==ERROR_SDCARD) {
    display.print("No SDCard found!");
  } 
  if (error_code==ERROR_PM_NODATA) {
    display.print("No data from PM sensor!");
  } 
  if (error_code==ERROR_RTC_INIT) {
    display.print("Clock not found");
  } 
  if (error_code==ERROR_NOTEMP) {
    display.print("No temperature sensor found!");
  } 
  if (error_code==ERROR_NO_SENSOR_ARD) {
    display.print("No connection to sensor Arduino!");
  } 
    
  display.display(); 
}
