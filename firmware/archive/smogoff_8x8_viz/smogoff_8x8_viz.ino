#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

const float ppb2ugm3 = 1.9125;

struct sensor_data_t {
  unsigned char id;
  long temp;
  long ppb;
  short pm25;
};

sensor_data_t sensor_data;
const uint8_t i2c_address = 9;

uint16_t read_sensor_data()
{
    uint16_t length=sizeof(sensor_data);
    uint16_t bytes_received = 0;
    uint16_t bytes_requested = min(length,16);

/*    Wire.beginTransmission(i2c_address);
    uint16_t location = 0;
    Wire.send((uint8_t)(location >> 8));
    Wire.send((uint8_t)(location & 0xff));
    Wire.endTransmission();
*/
    Wire.requestFrom(i2c_address,bytes_requested);
    uint16_t remaining = bytes_requested;
    uint8_t* next = (uint8_t*)&sensor_data;

    while (Wire.available() && remaining--)
    {
        *next++ = Wire.read();
        ++bytes_received;
    }

    return bytes_received;
}



Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

void setup() {
  Serial.begin(9600);
  Serial.println("8x8 LED Matrix Test");
  Wire.begin();
  
  matrix.begin(0x70);  // pass in the address
}

static const uint8_t PROGMEM
  smile_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10100101,
    B10011001,
    B01000010,
    B00111100 },
  neutral_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10111101,
    B10000001,
    B01000010,
    B00111100 },
  frown_bmp[] =
  { B00111100,
    B01000010,
    B10100101,
    B10000001,
    B10011001,
    B10100101,
    B01000010,
    B00111100 };

void test() {
  matrix.clear();
  matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_ON);
  matrix.writeDisplay();
  delay(500);

  matrix.clear();
  matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_ON);
  matrix.writeDisplay();
  delay(500);

  matrix.clear();
  matrix.drawBitmap(0, 0, frown_bmp, 8, 8, LED_ON);
  matrix.writeDisplay();
  delay(500);

  matrix.clear();      // clear display
  matrix.drawPixel(0, 0, LED_ON);  
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);

  matrix.clear();
  matrix.drawLine(0,0, 7,7, LED_ON);
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);

  matrix.clear();
  matrix.drawRect(0,0, 8,8, LED_ON);
  matrix.fillRect(2,2, 4,4, LED_ON);
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);

  matrix.clear();
  matrix.drawCircle(3,3, 3, LED_ON);
  matrix.writeDisplay();  // write the changes we just made to the display
  delay(500);

  matrix.setTextSize(1);
  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
  matrix.setTextColor(LED_ON);
  for (int8_t x=0; x>=-36; x--) {
    matrix.clear();
    matrix.setCursor(x,0);
    matrix.print("Then");
    matrix.writeDisplay();
    delay(100);
  }
  matrix.setRotation(3);
  for (int8_t x=7; x>=-36; x--) {
    matrix.clear();
    matrix.setCursor(x,0);
    matrix.print("Try");
    matrix.writeDisplay();
    delay(100);
  }
  matrix.setRotation(2);
  for (int8_t x=7; x>=-36; x--) {
    matrix.clear();
    matrix.setCursor(x,0);
    matrix.print("This");
    matrix.writeDisplay();
    delay(100);
  }
  matrix.setRotation(0);
}

void graph() {

  unsigned char v[8];
  int frame=0;
  while(frame<100) {
     
    for (int8_t x=0; x<7; x++) {
      v[x]=v[x+1];    
    }

    v[7]=int(8*(0.5+0.25*(sin(frame*0.98)+sin(frame*0.3))));
    frame++;
    
    matrix.clear();
    for (int8_t x=0; x<8; x++) {
      matrix.drawLine(x,0, x,v[x], LED_ON);
    }
    matrix.writeDisplay();
    delay(100);
  }
}

void show_face(uint8_t t) {
  matrix.clear();
  switch (t) {
    case 0:
      matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_ON);
      break;
    case 1: 
      matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_ON);
      break;
    case 2:
      matrix.drawBitmap(0, 0, frown_bmp, 8, 8, LED_ON);
      break;
    default:      
      matrix.drawPixel(0, 0, LED_ON);  
      break;
  }
  matrix.writeDisplay();
}

void loop() {
  if (read_sensor_data()==sizeof(sensor_data) && sensor_data.id==99) {  
    Serial.print(sensor_data.id);
    Serial.print(", ");
    Serial.print(sensor_data.ppb);
    Serial.print(", ");
    Serial.println(sensor_data.pm25);

    float no2 = ppb2ugm3*sensor_data.ppb;
    float pm = sensor_data.pm25;

    float no2_low = 7.0;
    float no2_high = 12.0;
    float pm_low = 3.0;
    float pm_high = 6.0;
    uint8_t face = 0;

    if (no2>no2_high) {
      face=2;
    } else {
      if (no2>no2_low) face=1;
    }    

    if (pm>pm_high) {
      face=2;
    } else {
      if (pm>pm_low && face!=2) face=1;
    }    

    show_face(face);
  } else {
    show_face(3);
  }
  
  delay(1000);



}
