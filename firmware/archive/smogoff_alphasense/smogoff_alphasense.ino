// Sonic Kayaks Copyright (C) 2020 FoAM Kernow
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <SPI.h>
#include <SD.h>
#include <SoftwareSerial.h>
#include "pms7003.h"
#include "RTClib.h"
#include "alpha.h"
 
const long PM_READ_DURATION = 10000;
const long PM_WARMUP_DURATION = 3000;
const long PM_WARMDOWN_DURATION = 1000;
const long PM_MAX_ERRORS = 50; // 50 read fails @ 100ms per attempt

#define STATE_PM_START 0
#define STATE_PM_WARMUP 1
#define STATE_PM_READ 2
#define STATE_PM_STOP 3
#define STATE_PM_WARMDOWN 4
#define STATE_NO2_READ 5

// globals
SoftwareSerial pmSerial(4, 5); // RX, TX
pms7003_stream pm_stream;
long timer=0;
unsigned char state = 0;

long temp;
long b;  
short pm25;

char zeroed = 0;

RTC_PCF8523 rtc;

void error_warning() {
  for (unsigned int i=0; i<1; i++) {
    digitalWrite(3,HIGH); 
    delay(100); 
    digitalWrite(3,LOW);
    delay(100);
  }
}

void display_ok() {
  for (unsigned int i=0; i<10; i++) {
    digitalWrite(3,HIGH); 
    delay(50); 
    digitalWrite(3,LOW);
    delay(50);
  }
}


void sdlog(int type, float a, float b, float c, float d) {  
  // see if the card is present and can be initialized:
  if (!SD.begin(10)) {
    error_warning();
    Serial.println("SD card not initialised!");    
  } 
  
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (dataFile) {
    DateTime now = rtc.now();
    dataFile.print(now.year(), DEC);
    dataFile.print('/');
    dataFile.print(now.month(), DEC);
    dataFile.print('/');
    dataFile.print(now.day(), DEC);
    dataFile.print(' ');
    dataFile.print(now.hour(), DEC);
    dataFile.print(':');
    dataFile.print(now.minute(), DEC);
    dataFile.print(':');
    dataFile.print(now.second(), DEC);
    dataFile.print(", "); 
    dataFile.print(millis());
    dataFile.print(", ");
    switch (type) {
      case 0: dataFile.print("pm, "); break;
      case 1: dataFile.print("no2, "); break;
      default: dataFile.print("system started, "); break;
    }
    dataFile.print(a,5);
    dataFile.print(", ");    
    dataFile.print(b,5);
    dataFile.print(", ");    
    dataFile.print(c,5);
    dataFile.print(", ");    
    dataFile.println(d,5);
    dataFile.close();

  } else {
    Serial.println("error opening datalog.txt");
    error_warning();
  }
    if (type==0) Serial.print("pm, ");
    else Serial.print("no2, ");
    Serial.print(a,5);
    Serial.print(", ");    
    Serial.print(b,5);
    Serial.print(", ");    
    Serial.print(c,5);
    Serial.print(", ");    
    Serial.println(d,5);
  
}

void check_pm() {
  unsigned long len=pmSerial.available();
  if (len > 0) {
    unsigned char buf[64]; 
    pmSerial.readBytes((char *)buf,len);
    pms7003_stream_update(&pm_stream,buf,len); 
    if (pm_stream.msg_ready==1 && pm_stream.msg_size==32) {
      pm_stream.msg_ready=0;  

      pm25 = swap_endian(pm_stream.msg.pmc_std_2_5);

      sdlog(0,swap_endian(pm_stream.msg.pmc_std_1_0),
              swap_endian(pm_stream.msg.pmc_std_2_5),
              swap_endian(pm_stream.msg.pmc_std_10_0),0);   
    } 
  }
}

// will block for ~one second
void read_no2() {
  float we_v;
  float aux_v;
  float ppb = alpha_read_ppb(we_v,aux_v);  
  b = ppb;
  temp = 0; 
  sdlog(1,0,we_v,aux_v,ppb);
}

void setup() {                
 
  //Serial.begin(9600);
   // init the i2c output
  pinMode(3,OUTPUT);
  Wire.begin(9);                // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event

  // init the air particulate matter sensor
  pms7003_stream_init(&pm_stream);
  pmSerial.begin(9600);
  // send it to sleep until it's needed
  pms7003_command cmd;
  pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_sleep);
  pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
  
  // test the sdcard
  SD.begin(10);
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (!dataFile) {
    error_warning();
  } 
  dataFile.close();

  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    error_warning();
  } 
  
  sdlog(3,0,0,0,0);
    
  state = STATE_PM_START;
  timer = millis();

  display_ok();
}


void loop() {  

  switch (state) {
    case STATE_PM_START: {
      Serial.println("starting pm");
      pms7003_command cmd;
      pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_wakeup);
      pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
      state=STATE_PM_WARMUP;
      timer=millis();
    } break;

    case STATE_PM_WARMUP: {
      if (millis()-timer>PM_WARMUP_DURATION) {
        state=STATE_PM_READ;
        timer=millis();
      } 
    } break;
    
    case STATE_PM_READ: {
      check_pm();
      if (millis()-timer>PM_READ_DURATION) {
        state=STATE_PM_STOP;
      } 
    } break;

    case STATE_PM_STOP: {
      Serial.println("stopping pm");
      pms7003_command cmd;
      pms7003_build_command(&cmd, pms7003_cmd_sleep, pms7003_data_sleep);
      pmSerial.write((const uint8_t *)&cmd,sizeof(cmd));
      state=STATE_PM_WARMDOWN;
      timer=millis();      
    } break;

    case STATE_PM_WARMDOWN: {
      if (millis()-timer>PM_WARMDOWN_DURATION) {
        state=STATE_NO2_READ;
      } 
    } break;


    case STATE_NO2_READ: {
      read_no2();
      state=STATE_PM_START;
    } break;

    default: break;
  }
  
  delay(100);
}

void requestEvent() {
  Wire.write(99);
  Wire.write((unsigned char *)&temp,sizeof(long));
  Wire.write((unsigned char *)&b,sizeof(long));  
  Wire.write((unsigned char *)&pm25,sizeof(short));
}
