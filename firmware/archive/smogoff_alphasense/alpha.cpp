#include "alpha.h"

float alpha_sample_diff_volts(int pina, int pinb, float secs) {
  unsigned long etime, i = 0;
  long anaCounts = 0;
  etime = millis() + secs * 1000;
  do {
    anaCounts = anaCounts + (analogRead(pina)-analogRead(pinb));
    delay(1);
    i++;
  } while (millis() < etime);
  float Cnts = float (anaCounts) / float(i);
  return Cnts * alpha_pVcc / 1024.0;
}

float alpha_read_ppb(float &we_v, float &aux_v) {
  we_v = alpha_sample_diff_volts(alpha_WEp_pin,alpha_WEn_pin,5);
  aux_v = alpha_sample_diff_volts(alpha_Auxp_pin,alpha_Auxn_pin,5);
  float ppm = ((we_v-WE_ZERO_V)-(aux_v-AUX_ZERO_V))/SENSITIVITY;
  return ppm*1000.0f;
}
