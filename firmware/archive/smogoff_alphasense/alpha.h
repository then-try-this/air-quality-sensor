#ifndef ALPHA_h
#define ALPHA_h

#include "Arduino.h"

const int alpha_WEp_pin=A0;
const int alpha_WEn_pin=A1;
const int alpha_Auxp_pin=A2;
const int alpha_Auxn_pin=A3;

const float alpha_pVcc = 4.99;

const float WE_ZERO_V = 0.218;
const float AUX_ZERO_V = 0.250;
const float SENSITIVITY = 0.271;

const float ppb2ugm3 = 1.9125;

float alpha_sample_diff_volts(int pina, int pinb, float secs);
float alpha_read_ppb(float &we_v, float &aux_v);

#endif
