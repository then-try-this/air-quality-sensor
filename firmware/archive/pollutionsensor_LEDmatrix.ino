#include <gfxfont.h>

#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_NeoMatrix.h>

#include <gamma.h>

// which pin on the Arduino is connected to the NeoPixels?
#define PIN 6

#define SENSOR_PIN A0

#define GRID_COLS 8
#define GRID_ROWS 8
#define START_POSX 1
#define START_POSY 1

#define LUNG_BLOCKS 10
#define STOP 10 // the number at which to stop destroying lungs and restart
#define CAR_BLOCKS 3
#define NUM_BALLS 10

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(GRID_COLS, GRID_ROWS, PIN,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

#define OFF matrix.Color(0, 0, 0)
#define BLUE matrix.Color(150, 0, 150)
#define WHEELS matrix.Color(120, 120, 0)
#define CRIMSON matrix.Color(175, 0, 0)
#define GREY matrix.Color(128, 15, 0)
#define WHITE matrix.Color(255, 255, 255)

// global constants
int lung_counter = LUNG_BLOCKS;
int led = 13;
bool triggered = false;

struct ball;

void debug(const char* s, ball* b);
void initial(void);


struct ball {
  int id = 0;

  int startx = 0;
  int starty = 0;

  int x = 0;
  int y = 0;

  int dx = 0; // in {-1, 0, 1}
  int dy = 0; // in {-1, 0, 1}

uint32_t colour;
  
  void draw(void) {
    matrix.drawPixel(this->x, this->y, this->colour);
     matrix.drawPixel(this->x+1, this->y+1, this->colour);
      matrix.drawPixel(this->x+1, this->y, this->colour);
       matrix.drawPixel(this->x, this->y+1, this->colour);
  }

  void clear() { 
     // set its current position to OFF
     matrix.drawPixel(this->x, this->y, OFF);
     matrix.drawPixel(this->x+1, this->y+1, OFF);
      matrix.drawPixel(this->x+1, this->y, OFF);
       matrix.drawPixel(this->x, this->y+1, OFF);
  
  }
  
  void reset(void) {
   matrix.drawPixel(this->x, this->y, OFF);

   this->x = this->startx;
   this->y = this->starty;

   this->dx = 1;
   this->dy = -1;

   this->draw();
  }

  bool move(void) {
     if (this->dx == 0) {
       debug("WHAT THE FUCK", this);
     }

  
     // randomness
     if (random(0, 1000) < 2) {
       this->dy *= -1;
     }

     if (random(0, 1000) < 16) {
      this->dx *= -1;
     }

     // update its position
     this->x += dx;
     this->y += dy;

    
     // if we're at the edges of the grid, adjust our velocities
     if (this->x == 0 || this->x == GRID_COLS - 2) {
       debug("X FLIP", this);

       this->dx *= -1;
     }

     if (this->y == 0 || this->y == GRID_ROWS - 2) {
      debug("Y FLIP", this);
      this->dy *= -1;
     }

     this->x = max(min(this->x, GRID_COLS-2), 0);
     this->y = max(min(this->y, GRID_ROWS-2), 0);

     this->draw();

     return true;
  };


  ball(int id, int init_x, int init_y) {
    this->id = id;
    this->startx = init_x;
    this-> colour = matrix.Color(random(0,255), random(0,255), random(0,255));
    this->starty = init_y;
    this->reset();
  }

  ball(int init_x, int init_y) {
    this->startx = init_x;
    this->starty = init_y;
    this->reset();
  }
};

ball* balls[NUM_BALLS];
int reading;

void setup(void)
{
  Serial.begin(9600);

  pinMode(led, OUTPUT);

  matrix.begin();
  matrix.setBrightness(15);
  matrix.fillScreen(0);
  matrix.show();
  initial();

  for (int i = 0; i < NUM_BALLS; i++) {
    balls[i] = new ball(i, random(0,7), random(0,7));
  }

  triggered = false;
  Serial.println("INIT COMPLETE");
}

int cnt = 0;


void loop(void)
{
  cnt = analogRead(SENSOR_PIN)/100;
     /* for (int i = 0; i < cnt; i++) {
      balls[i]->clear();
    } */
    matrix.fillScreen(0);
    for (int i = 0; i < cnt; i++) {
      if (!balls[i]->move()) {
        break;
      }
    }

      matrix.show();
    
    delay(100);
  
}

// HELPER FUNCTIONS
void debug(const char* s, ball* b) {
 
}

void initial(void) {


  // reset the ball to its starting position
  for (int i = 0; i < NUM_BALLS; i++) {
    balls[i]->reset();
}}
