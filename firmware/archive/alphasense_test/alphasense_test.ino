void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

const int WEp_pin=A0;
const int WEn_pin=A1;
const int Auxp_pin=A2;
const int Auxn_pin=A3;

const float pVcc = 4.99;

float sample_diff_volts(int pina, int pinb, float secs) {
  unsigned long etime, i = 0;
  unsigned long anaCounts = 0;
  etime = millis() + secs * 1000;
  do {
    anaCounts = anaCounts + analogRead(pina);
    delay(1);
    i++;
  } while (millis() < etime);
  float Cnts = float (anaCounts) / float(i);
  return Cnts * pVcc / 1024.0;
}

/*float SN_TOTAL_WE = 0.1720;
float SN_TOTAL_AE = 0.2240;
float SN_SENSITIVITY = 0.2080;
*/
float WE_ZERO_V = 0.218;
float AUX_ZERO_V = 0.250;
float SENSITIVITY = 0.271;

float ppb2ugm3 = 1.9125;

float read_ppb() {
  float we_v = sample_diff_volts(WEp_pin,WEn_pin,1);
  float aux_v = sample_diff_volts(Auxp_pin,Auxn_pin,1);
  float ppm = ((we_v-WE_ZERO_V)-(aux_v-AUX_ZERO_V))/SENSITIVITY;
  return ppm*1000.0f;
}

void loop() {
  Serial.println(read_ppb(),6);
}
