// Wire Master Reader
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Reads data from an I2C/TWI slave device
// Refer to the "Wire Slave Sender" example for use with this

// Created 29 March 2006

// This example code is in the public domain.

#include <Wire.h>

struct sensordata_t {
  char id;
  long temp;
  long n02;  
  int pm1;
  int pm25;
  int pm10;
};

sensordata_t sensordata;

void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(9600);  // start serial for output
}

template <typename T> unsigned int I2Cread(T& value) {
  byte * p = (byte*) &value;
  unsigned int i;
  for (i = 0; i < sizeof value; i++)
        *p++ = Wire.read();
  return i;
}

void loop() {
  Wire.requestFrom(9, 1); 
  Serial.print((int)Wire.read());
  
  Wire.requestFrom((unsigned char)9, (unsigned int)sizeof(sensordata)); 

  I2Cread<sensordata_t>(sensordata);

  Serial.print((int)sensordata.id);
  Serial.print(", ");
  Serial.print((long)sensordata.temp);
  Serial.print(", ");
  Serial.print((long)sensordata.n02);
  Serial.print(", ");
  Serial.print((int)sensordata.pm1);
  Serial.print(", ");
  Serial.print((int)sensordata.pm25);
  Serial.print(", ");
  Serial.println((int)sensordata.pm10);

  delay(500);
}
