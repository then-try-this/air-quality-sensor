#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

const float ppb2ugm3 = 1.9125;
#define LED_PIN     6
#define LED_COUNT  24
#define BRIGHTNESS 50 // Set BRIGHTNESS to about 1/5 (max = 255)
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

struct sensor_data_t {
  unsigned char id;
  long temp;
  long ppb;
  short pm25;
};

sensor_data_t sensor_data;
const uint8_t i2c_address = 9;

uint16_t read_sensor_data()
{
    uint16_t length=sizeof(sensor_data);
    uint16_t bytes_received = 0;
    uint16_t bytes_requested = min(length,16);

/*    Wire.beginTransmission(i2c_address);
    uint16_t location = 0;
    Wire.send((uint8_t)(location >> 8));
    Wire.send((uint8_t)(location & 0xff));
    Wire.endTransmission();
*/
    Wire.requestFrom(i2c_address,bytes_requested);
    uint16_t remaining = bytes_requested;
    uint8_t* next = (uint8_t*)&sensor_data;

    while (Wire.available() && remaining--)
    {
        *next++ = Wire.read();
        ++bytes_received;
    }

    return bytes_received;
}

void setup() {
  Wire.begin();
  Serial.begin(9600);
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(BRIGHTNESS);
}

int t=0;
void loop() {
  if (read_sensor_data()==sizeof(sensor_data) && sensor_data.id==99) {  
    Serial.print(sensor_data.id);
    Serial.print(", ");
    Serial.print(sensor_data.ppb);
    Serial.print(", ");
    Serial.println(sensor_data.pm25);

    float no2 = ppb2ugm3*sensor_data.ppb;
    bars(no2/20.0,sensor_data.pm25/10.0);
  } else {
    rainbowFade2White(3, 3, 1);
  }
  delay(1000);
}

void bars(float x, float y) {
  uint8_t height = LED_COUNT/2;
  for(int j=0; j<=height; j++) { 
    uint8_t b=(j/float(height))*127;
    if (j<=x*height) {
       strip.setPixelColor(j, strip.Color(255, b, b, 255));
    } else {
       strip.setPixelColor(j, strip.Color(0, 0, 0, 255));
    }
    strip.show();
  }
  for(int j=0; j<=height; j++) { 
    uint8_t b=(j/float(height))*127;
    if (j<=y*height) {
       strip.setPixelColor(LED_COUNT-j, strip.Color(b, 255, b, 255));
    } else {
       strip.setPixelColor(LED_COUNT-j, strip.Color(0, 0, 0, 255));
    }
    strip.show();
  }
}

void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops) {
  int fadeVal=0, fadeMax=100;

  // Hue of first pixel runs 'rainbowLoops' complete loops through the color
  // wheel. Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to rainbowLoops*65536, using steps of 256 so we
  // advance around the wheel at a decent clip.
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*65536;
    firstPixelHue += 256) {

    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    strip.show();
    delay(wait);

    if(firstPixelHue < 65536) {                              // First loop,
      if(fadeVal < fadeMax) fadeVal++;                       // fade in
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) { // Last loop,
      if(fadeVal > 0) fadeVal--;                             // fade out
    } else {
      fadeVal = fadeMax; // Interim loop, make sure fade is at max
    }
  }

  for(int k=0; k<whiteLoops; k++) {
    for(int j=0; j<256; j++) { // Ramp up 0 to 255
      // Fill entire strip with white at gamma-corrected brightness level 'j':
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
    delay(1000); // Pause 1 second
    for(int j=255; j>=0; j--) { // Ramp down 255 to 0
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
  }
}
