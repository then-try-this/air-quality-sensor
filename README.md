# Air Quality Sensors Project

Project website: https://thentrythis.org/projects/smogoff/

Funded by a [Smartline In Residence Scheme](https://www.smartline.org.uk/) grant, and the UKRI Doctoral Career Development Fund with Dr Antony Brown.
