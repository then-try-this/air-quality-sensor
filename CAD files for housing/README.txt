### CAD files for housing ###

Within this folder are DXF files (to be opened in LibreCad).

"Acrylic version Master" contains all CAD shapes required to build the SmogOff housing:
- Acrylic parts (front and back panels, bottom grill, shelf, clip).
- Aluminium mounting bracket.
- MDF block mould for bending the canopy.
- MDF register to hold front and back panels when CNC routing the grooves.