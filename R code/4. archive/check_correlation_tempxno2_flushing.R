##################################################
#### CODE FOR PLOTTING PM AND NO2 SENSOR DATA ####
##################################################

rm(list=ls()) # clear R's brain
setwd("~/GitHub/SmogOff/data/smartlinedata/013-2020-12-20-B") # set working directory (where data is stored)

library(tidyverse) # load tidyverse package
library(ggplot2) # load ggplot2 package
library(lubridate) # load lubridate package
library(scales) # load scales package 

temp <- read.table("DATALOG.TXT", sep=",", skip=1, header=F) # read data
temp <- temp %>% 
  rename(datetime = V1,
         millisec_since_startup = V2,
         type = V3) %>%# rename columns
  mutate(datetime = ymd_hms(datetime),
         type = as.character(type))

temp$type[which(temp$type == " no2")] <- "no2" 
temp$type[which(temp$type == " pm")] <- "pm"
temp <- temp %>% 
  mutate(type = as.factor(type)) # clunky way to remove space before names of type
temp <- temp %>% filter(type == "no2")

## filter out first hour since startup
temp <- temp %>%
  filter(millisec_since_startup >=3600000)
## filter out time after collection 
# temp <- temp %>% 
#   filter(datetime <= "2022-03-23 17:00:00")
temp <- temp %>% 
  select(-c(V5,V6)) %>% 
  rename(value = V7,
         temp = V4)
temp <- temp %>% 
  mutate(temp = scale(temp))


setwd("~/GitHub/SmogOff/data/smartlinedata/013-2021-12-20-A") # set working directory (where data is stored)

data <- read.table("DATALOG.TXT", sep=",", skip=1, header=F) # read data

data <- data %>% 
  rename(datetime = V1,
         millisec_since_startup = V2,
         type = V3) %>%# rename columns
  mutate(datetime = ymd_hms(datetime),
         type = as.character(type))

data$type[which(data$type == " no2")] <- "no2" 
data$type[which(data$type == " pm")] <- "pm"
data <- data %>% 
  mutate(type = as.factor(type)) # clunky way to remove space before names of type

## filter out first hour since startup
data <- data %>%
  filter(millisec_since_startup >=3600000)
## filter out time after collection 
# data <- data %>% 
#   filter(datetime <= "2022-03-23 17:00:00")

## split to isolate correct reading for pm and no2 and then recombine back to one dataset
pm <- data %>% 
  filter(type == "pm") %>% 
  select(-c(V4,V6,V7)) %>% 
  rename(value = V5) %>%
  mutate(temp = NA) %>% 
  relocate(temp, .before = value)
no2 <- data %>% 
  filter(type == "no2") %>% 
  select(-c(V5,V6)) %>% 
  rename(value = V7,
         temp = V4)
data <- bind_rows(no2,pm)

## filter out negative and large values
data <- data %>%
  mutate(filter_condition = ifelse(type == "pm" & value < 0, "remove", NA),
         filter_condition = ifelse(type == "no2" & value > 150, "remove", filter_condition)) %>% 
  filter(is.na(filter_condition) == T)

#### RUN THIS TO CALCULATE MINUTELY MEAN...

data <- data %>%
  mutate(minute = ymd_hm(substr(datetime, 1, 16)) + seconds(30)) # create column identifying day + hour + minute of reading, added 30 seconds as midpoint for plotting

## scale
data <- data %>%
  mutate(value = scale(value),
         temp = scale(temp))

no2 <- data %>%
  filter(type == "no2") %>% 
  group_by(minute) %>%
  mutate(mean = mean(value)) %>% # calculate minutely mean
  ungroup()
pm <- data %>%
  filter(type == "pm") %>% 
  group_by(minute) %>%
  mutate(mean = mean(value)) %>% # calculate minutely mean
  ungroup()

no2_minute <- no2 %>%
  group_by(minute) %>%
  slice(1) # collapse dataframe down to one value per hour for plotting
pm_minute <- pm %>%
  group_by(minute) %>%
  slice(1) # collapse dataframe down to one value per hour for plotting
minutely <- bind_rows(no2_minute, pm_minute)

#### PLOT hourly mean
data <- data %>% filter(datetime >= "2021-12-20 18:00:00")
temp <- temp %>% filter(datetime >= "2021-12-20 18:00:00")
plot1 <- ggplot()+
  # geom_point(data=pm,
  #            aes(x=datetime,
  #                y=value), alpha = 0.25, colour =  "skyblue")+
  # geom_point(data=pm_minute, aes(x=minute, y= mean), alpha = 1.0, size = 1, colour = "darkblue")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=value), alpha = 0.25, colour = "green")+
  geom_point(data = temp, aes(x=datetime, y=temp))+
  geom_point(data=no2_minute, aes(x=minute, y= mean), alpha = 1.0, size = 1, colour = "darkgreen")+
  geom_line(data=no2_minute, aes(x=minute, y = mean), colour = "darkgreen")+
  # geom_line(data=pm_minute, aes(x=minute, y = mean), colour="darkblue")+
  # geom_hline(yintercept = 13, linetype = 1, colour = "green")+ ## no2 WHO hourly limit in ug/m3
  # geom_hline(yintercept = 15, linetype = 1, colour = "blue")+ ## pm WHO hourly limit in ug/m3
  # annotate("text", x = ymd_hms("2022-03-23 10:00:00"), hjust =0, y=100, size = 4, colour = "darkgreen", label = 'paste("24hr mean limit ", NO[2], " = 13ppb", sep=" ")', parse = T) + ### CHANGE DATE-TIME TO POSITION TEXT ACCORDINGLY
  # annotate("text", x = ymd_hms("2022-03-23 10:00:00"), hjust =0, y=110, size = 4, colour = "darkblue", label = 'paste("24hr mean limit ", PM2.5, " = 15\u03bcg/", m^3, sep=" ")', parse = T) + ### CHANGE DATE-TIME TO POSITION TEXT ACCORDINGLY
  xlab("Time")+
  scale_y_continuous(name = expression(paste("PM2.5 concentration (\u03bcg/", "m"^"3", ")", sep=" ")), breaks = seq(0,150,10), limits = c(-5,5),
                     sec.axis = sec_axis(~., name = bquote(NO[2]~concentration~(ppb)), breaks = seq(0,150,10)))+
  scale_x_datetime(breaks = date_breaks("30 min"), labels = date_format("%H:%M"))+
  theme_bw()

plot1




