######################################
#### CALCULATE UNCORRECTED VALUES ####
######################################

rm(list=ls()) # clear R's brain
setwd("~/GitHub/SmogOff/data") # set working directory (where data is stored)

library(tidyverse) # load tidyverse package
library(ggplot2) # load ggplot2 package
library(lubridate) # load lubridate package
library(scales) # load scales package 

data <- read.table("07.05.2022_batterytest_Lithion_collegehillpatio.TXT", sep=",", skip=2, header=F) # read data

data <- data %>% 
  rename(datetime = V1,
         millisec_since_startup = V2,
         type = V3) %>%# rename columns
  mutate(datetime = ymd_hms(datetime),
         type = as.character(type))

data$type[which(data$type == " no2")] <- "no2" 
data$type[which(data$type == " pm")] <- "pm"
data <- data %>% 
  mutate(type = as.factor(type)) # clunky way to remove space before names of type

## filter out first hour since startup
data <- data %>%
  filter(millisec_since_startup >=3600000)
## filter out time after collection 
# data <- data %>% 
#   filter(datetime <= "2022-03-23 17:00:00")

#### create no2 dataframe for converting back to uncorrected values
no2u <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
  rename(we_mv = V5, aux_mv = V6) %>% 
  mutate(uncorrectedNO2 = ((we_mv-218)-(aux_mv-250))/0.271) %>% 
  mutate(uncorrectedNO2 = uncorrectedNO2*1.9125)

#### create no2 dataframe for converting algo3 to algo3 with no correction
no2u3 <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
  rename(we_mv = V5, aux_mv = V6) %>% 
  mutate(uncorrectedAlgo3 = ((we_mv-218)-((208-218)-(241-250))-(aux_mv-250))/0.271) %>% 
  mutate(uncorrectedAlgo3 = uncorrectedAlgo3*1.9125)


#### create no2 dataframe for converting to algorithm 1
no2a1 <- data %>% filter(type=="no2") %>% select(-V7) %>% 
  rename(we_mv = V5, aux_mv = V6, temp = V4) %>% 
  mutate(temp = round(temp, digits=1)) %>% 
  mutate(temp = as.character(temp))

# create dataframe using correction values based on Application Note AAN-803-05
a <- data.frame(temp = c(-30,-20,-10,0,10,20,30,40,50), correction=c(1.3,1.3,1.3,1.3,1,0.6,0.4,0.2,-1.5)) # From table 3 in application note document

func <- splinefun(x=a$temp, y = a$correction,
                  method = "fmm",
                  ties = mean) ## create spline to fit through values 
corrected_val <- data.frame(n = seq(1,801,1),correction = func(seq(-30, 50, 0.1)))

b <- data.frame(n = seq(1,801,1),temp = seq(-30,50,0.1))

lookup <- left_join(x=b, y= corrected_val, by="n") %>% mutate(temp=as.character(temp))
lookup <- lookup %>% 
  mutate(temp = as.numeric(temp),
         temp = round(temp, digits= 1),
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))

no2a1 <- no2a1 %>% 
  mutate(temp = as.numeric(temp), 
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))
no2a1 <- left_join(x=no2a1, y=lookup, by="temp_charactercode")
no2a1 <- no2a1 %>% select(-n) 

no2a1 <- no2a1 %>% 
  mutate(algo1 = ((we_mv-218)-(correction*(aux_mv-250)))/0.271) %>% 
  mutate(algo1 = algo1*1.9125)


## split to isolate correct reading for pm and no2 and then recombine back to one dataset
no2 <- data %>% 
  filter(type == "no2") %>%
  rename(we_mv = V5, aux_mv = V6,algo3 = V7,temp = V4)

no2a1 <- no2a1 %>% select(millisec_since_startup, algo1)
no2u <- no2u %>% select(millisec_since_startup, uncorrectedNO2)
no2u3 <- no2u3 %>% select(millisec_since_startup, uncorrectedAlgo3)

no2 <- left_join(x=no2, y=no2a1, by="millisec_since_startup")
no2 <- left_join(x=no2, y=no2u, by="millisec_since_startup")
no2 <- left_join(x=no2, y=no2u3, by="millisec_since_startup")

data <- data %>%
  mutate(minute = ymd_hm(substr(datetime, 1, 16)) + seconds(30)) # create column identifying day + hour + minute of reading, added 30 seconds as midpoint for plotting


#### PLOT 
plot1 <- ggplot()+
  geom_point(data=no2,
             aes(x=datetime,
                 y=uncorrectedNO2), alpha = 0.25, colour =  "skyblue")+
  geom_point(data=no2,
  aes(x=datetime,
      y=algo1), alpha = 0.25, colour =  "forestgreen")+
  geom_point(data=no2,
  aes(x=datetime,
      y=algo3), alpha = 0.25, colour =  "orange")+
  # geom_point(data=no2,
  #            aes(x=datetime,
  #                y=uncorrectedAlgo3), alpha = 0.25, colour =  "red")+
  geom_smooth(data=no2, aes(x=datetime, y=uncorrectedNO2),method="loess", span =0.1, colour="skyblue", se=F, size=1.5)+
  geom_smooth(data=no2, aes(x=datetime, y=algo1),method="loess", span =0.1, colour="forestgreen", se=F, size=1.5)+
  geom_smooth(data=no2, aes(x=datetime, y=algo3),method="loess", span =0.1, colour="orange", se=F, size=1.5)+
  # geom_smooth(data=no2, aes(x=datetime, y=uncorrectedAlgo3),method="loess", span =0.1, colour="red", se=F, size=1.5)+
  scale_y_continuous(name = expression(paste(NO[2], " concentration (\u03bcg/ ", "m"^"3", ")", sep=" ")), breaks = seq(-200,300,50), limits = c(-100,100))+
  scale_x_datetime(breaks = date_breaks("30 min"), labels = date_format("%H:%M"))+
  annotate(geom="text", x= min(no2$datetime), hjust=0, y=min(no2$uncorrectedNO2), label ="uncorrected", colour="skyblue", size=10)+
  annotate(geom="text", x= mean(no2$datetime), hjust=0, y=min(no2$algo1), label ="algorithm 1", colour="forestgreen", size=10)+
  annotate(geom="text", x= min(no2$datetime), hjust=0, y=min(no2$algo3), label ="algorithm 3", colour="orange", size=10)+
  # annotate(geom="text", x= min(no2$datetime), hjust=0, y=min(no2$uncorrectedAlgo3), label ="algorithm 3 (uncorrected)", colour="red", size=10)+
  theme_bw()

plot1


### 
flow <- read.csv("07.05.2022_plumeflow_collegehillpatio.csv")
flow <- flow %>% 
  mutate(date..UTC. = ymd_hms(date..UTC.)) %>% 
  mutate(date.BST = date..UTC.+hours(1)) %>% 
  mutate(NO2 = NO2..ppb.*1.9125) %>% 
  filter(date.BST >= "2022-05-07 13:45:00" & date.BST <= "2022-05-08 15:10:00")

#### PLOT 
plot1 <- ggplot()+
  geom_point(data=flow,
             aes(x=date.BST,
                 y=NO2), alpha = 0.25, colour =  "black")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=uncorrectedNO2), alpha = 0.25, colour =  "skyblue")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=algo1), alpha = 0.25, colour =  "forestgreen")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=algo3), alpha = 0.25, colour =  "orange")+
  # geom_point(data=no2,
  #            aes(x=datetime,
  #                y=uncorrectedAlgo3), alpha = 0.25, colour =  "red")+
  geom_smooth(data=no2, aes(x=datetime, y=uncorrectedNO2),method="loess", span =0.01, colour="skyblue", se=F, size=1.5)+
  geom_smooth(data=no2, aes(x=datetime, y=algo1),method="loess", span =0.01, colour="forestgreen", se=F, size=1.5)+
  geom_line(data=flow, aes(x=date.BST, y=NO2),method="loess", span =0.01, colour="black", se=F, size=1.0, alpha=0.5)+
  geom_smooth(data=no2, aes(x=datetime, y=algo3),method="loess", span =0.01, colour="orange", se=F, size=1.5)+
  # geom_smooth(data=no2, aes(x=datetime, y=uncorrectedAlgo3),method="loess", span =0.1, colour="red", se=F, size=1.5)+
  scale_y_continuous(name = expression(paste(NO[2], " concentration (\u03bcg/ ", "m"^"3", ")", sep=" ")), breaks = seq(-200,300,50), limits = c(-75,100))+
  scale_x_datetime(breaks = date_breaks("2 hour"), labels = date_format("%H:%M"))+
  annotate(geom="text", x= mean(no2$datetime), hjust=0, y=min(-30), label ="uncorrected", colour="skyblue", size=5)+
  annotate(geom="text", x= mean(no2$datetime), hjust=0, y=min(-40), label ="algorithm 1", colour="forestgreen", size=5)+
  annotate(geom="text", x= mean(no2$datetime), hjust=0, y=min(-50), label ="algorithm 3", colour="orange", size=5)+
  # annotate(geom="text", x= min(no2$datetime), hjust=-0.1, y=min(no2$uncorrectedAlgo3), label ="algorithm 3", colour="red", size=5)+
  annotate(geom="text", x= mean(no2$datetime), hjust=0, y=min(flow$NO2-20), label ="Plume Flow 2", colour="black", size=5)+
  annotate(geom="text", x= mean(no2$datetime), hjust=1, y=min(90), label ="7th-8th May 2022", colour="black", size=5)+
  xlab("")+
  theme_bw()

plot1

