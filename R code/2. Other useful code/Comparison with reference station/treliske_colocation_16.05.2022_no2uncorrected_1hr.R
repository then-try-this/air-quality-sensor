###################################################
#### TRELISKE COMPARISON 16th to 17th May 2022 ####
###################################################

### not temperature corrected ###

rm(list=ls()) # clear R's brain
setwd("~/GitHub/SmogOff/data") # set working directory (where data is stored)

library(tidyverse) # load tidyverse package
library(ggplot2) # load ggplot2 package
library(lubridate) # load lubridate package
library(scales) # load scales package 

data <- read.table("16.05.2022_colocation_Treliske.TXT", sep=",", skip=2, header=F) # read data

data <- data %>% 
  rename(datetime = V1,
         millisec_since_startup = V2,
         type = V3) %>%# rename columns
  mutate(datetime = ymd_hms(datetime),
         type = as.character(type))

data$type[which(data$type == " no2")] <- "no2" 
data$type[which(data$type == " pm")] <- "pm"
data <- data %>% 
  mutate(type = as.factor(type)) # clunky way to remove space before names of type

## filter out first hour since startup
# data <- data %>%
#   filter(millisec_since_startup >=3600000)
## filter out time after collection 
# data <- data %>% 
#   filter(datetime <= "2022-03-23 17:00:00")

#### create no2 dataframe for converting back to uncorrected values
no2u <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
  rename(we_mv = V5, aux_mv = V6) %>% 
  mutate(uncorrectedNO2 = ((we_mv-218)-(aux_mv-250))/0.271) %>% 
  mutate(uncorrectedNO2 = uncorrectedNO2*1.9125)

# #### create no2 dataframe for converting algo3 to algo3 with no correction
# no2u3 <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
#   rename(we_mv = V5, aux_mv = V6) %>% 
#   mutate(uncorrectedAlgo3 = ((we_mv-218)-((208-218)-(241-250))-(aux_mv-250))/0.271) %>% 
#   mutate(uncorrectedAlgo3 = uncorrectedAlgo3*1.9125)


#### create no2 dataframe for converting to algorithm 1
no2a1 <- data %>% filter(type=="no2") %>% select(-V7) %>% 
  rename(we_mv = V5, aux_mv = V6, temp = V4) %>% 
  mutate(temp = round(temp, digits=1)) %>% 
  mutate(temp = as.character(temp))

# create dataframe using correction values based on Application Note AAN-803-05
a <- data.frame(temp = c(-30,-20,-10,0,10,20,30,40,50), correction=c(1.3,1.3,1.3,1.3,1,0.6,0.4,0.2,-1.5)) # From table 3 in application note document

##### CHANGE THIS !!! NEEDS TO BE LINEAR
func <- approxfun(x=a$temp, y=a$correction,method="linear", ties=mean)

# func <- splinefun(x=a$temp, y = a$correction,
#                   method = "fmm",
#                   ties = mean) ## create spline to fit through values 
corrected_val <- data.frame(n = seq(1,801,1),correction = func(seq(-30, 50, 0.1)))

b <- data.frame(n = seq(1,801,1),temp = seq(-30,50,0.1))


lookup <- left_join(x=b, y= corrected_val, by="n") %>% mutate(temp=as.character(temp))
lookup <- lookup %>% 
  mutate(temp = as.numeric(temp),
         temp = round(temp, digits= 1),
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))

plot <- ggplot()+
  geom_point(data=lookup,
             aes(x=temp,
                 y=correction), alpha = 0.25, colour =  "black")+
  geom_point(data=a,
             aes(x=temp,
                 y=correction), alpha = 1, size=5, colour =  "red")
plot

no2a1 <- no2a1 %>% 
  mutate(temp = as.numeric(temp), 
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))
no2a1 <- left_join(x=no2a1, y=lookup, by="temp_charactercode")
no2a1 <- no2a1 %>% select(-n) 

no2a1 <- no2a1 %>% 
  mutate(algo1 = ((we_mv-218)-(correction*(aux_mv-250)))/0.271) %>% 
  mutate(algo1 = algo1*1.9125)


## split to isolate correct reading for pm and no2 and then recombine back to one dataset
no2 <- data %>% 
  filter(type == "no2") %>%
  rename(we_mv = V5, aux_mv = V6,algo3 = V7,temp = V4)

no2a1 <- no2a1 %>% select(millisec_since_startup, algo1)
no2u <- no2u %>% select(millisec_since_startup, uncorrectedNO2)
# no2u3 <- no2u3 %>% select(millisec_since_startup, uncorrectedAlgo3)

no2 <- left_join(x=no2, y=no2a1, by="millisec_since_startup")
no2 <- left_join(x=no2, y=no2u, by="millisec_since_startup")
# no2 <- left_join(x=no2, y=no2u3, by="millisec_since_startup")



#### rolling average

no2$rollmean <- sapply(X = seq_along( no2$uncorrectedNO2 ), 
                         FUN = function(x) { 
                           mean( no2$uncorrectedNO2[ no2$datetime >= no2$datetime[x] - 60*60 & 
                                              no2$datetime <= no2$datetime[x] ] ) 
                         } )


setwd("U:/DOCTORAL CAREER DEVELOPMENT FUND/TreliskeColocationData")
reference <- read.csv("16.05.2022_1hrmean.csv")

reference <- reference %>% 
  mutate(Date=as.character(Date),
         Time = as.character(Time))

reference$Time <- ifelse(reference$Time == "24:00:00", "00:00", reference$Time)

reference <- reference %>% 
         mutate(datetime = paste(Date, Time, sep=" "))

reference$datetime <- ifelse(reference$datetime == "16/05/2022 00:00", "17/05/2022 00:00", reference$datetime)

reference <- reference %>% 
  mutate(datetime = dmy_hm(datetime))



# 1 hour mean

time_intervals <- reference %>% select(datetime) %>% rename(datetime_start = datetime) %>% 
  mutate(datetime_end = datetime_start + (60*60)-1)

### LOOP --> FOR EACH TURNOVER ID IN "turn" DATA, RETURN ALL BIRDS WHOSE CAREER ENTRY OVERLAPS THAT PREV DOM EST END DATE ###

data_list <- as.list(NA) # list to store data

# progress bar
pbar <- txtProgressBar(max = nrow(no2), style = 3)

for(i in 1:nrow(no2)) { 
  interval <- time_intervals$datetime_start[which(no2$datetime[i] %within% interval(time_intervals$datetime_start, time_intervals$datetime_end))]
  df <- merge(x = no2[i,], y = data.frame(interval))
  data_list[[i]] <- df  
  setTxtProgressBar(pb = pbar, value = i)
}

library(data.table)
new_data <- rbindlist(data_list) 
rm(data_list, pbar, df)

no2 <- new_data

### 1hr mean
mean <- no2 %>% 
  group_by(interval) %>% 
  summarise(mean1hr = mean(uncorrectedNO2))
mean <- mean %>% 
  mutate(interval = interval + (60*60))
no2 <- no2 %>% 
  mutate(interval = interval + (60*60))
no2 <- left_join(x=no2,y=mean, by="interval")

### 1hr max
max <- no2 %>% 
  group_by(interval) %>% 
  summarise(max1hr = max(uncorrectedNO2))

no2 <- left_join(x=no2,y=max, by="interval")

#####

### 
# flow <- read.csv("10.05.2022_plumeflow_collegehillpatio.csv")
# flow <- flow %>% 
#   mutate(date..UTC. = ymd_hms(date..UTC.)) %>% 
#   mutate(date.BST = date..UTC.+hours(1)) %>% 
#   mutate(NO2 = NO2..ppb.*1.9125) %>% 
#   filter(date.BST >= "2022-05-10 12:33:00" & date.BST <= "2022-05-11 10:53:00")
# 
# flow2 <- read.csv("10.05.2022_plumeflow_collegehillpatio.csv")
# flow2 <- flow2 %>% 
#   mutate(date..UTC. = ymd_hms(date..UTC.)) %>% 
#   mutate(date.BST = date..UTC.+hours(1)) %>% 
#   mutate(NO2 = NO2..ppb.*1.9125) %>% 
#   filter(date.BST >= "2022-05-11 13:16:00" & date.BST <= "2022-05-11 22:55:00")

# flow <- bind_rows(flow,flow2)


#### PLOT NO2 uncorrected

plot1 <- ggplot()+
  
  geom_point(data=reference,
             aes(x=datetime,
                 y=NO2ugm3), alpha = 1, colour =  "black")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=uncorrectedNO2), alpha = 0.25, colour =  "skyblue")+
  geom_point(data = no2,
             aes(x=interval,
                 y=mean1hr), alpha = 1, colour = "blue")+
  # geom_point(data = no2,
  #            aes(x=interval,
  #                y=max1hr), alpha = 1, colour = "darkblue")+
  # geom_point(data = no2,
  #            aes(x=interval,
  #                y=rollmean), alpha = 1, colour = "blue")+

  geom_smooth(data=no2, aes(x=datetime, y=mean1hr),method="loess", span =0.2, colour="blue", se=F, size=1)+
  geom_smooth(data=reference, aes(x=datetime, y=NO2ugm3),method="loess", span =0.2, colour="black", se=F, size=1)+

  scale_y_continuous(name = expression(paste(NO[2], " concentration (\u03bcg/ ", "m"^"3", ")", sep=" ")), breaks = seq(-200,300,10), limits = c(-50,75))+
  scale_x_datetime(breaks = date_breaks("3 hour"), labels = date_format("%H:%M"))+
  xlab("")+
  annotate("text", x = ymd_hms("2022-05-16 10:00:00"), y = 70, label = "NO2", size=8)+
  annotate("text", x = ymd_hms("2022-05-17 03:00:00"), y = 70, label = "SmogOff 15 minute mean", colour="darkblue")+
  annotate("text", x = ymd_hms("2022-05-17 03:00:00"), y = 65, label = "Treliske reference monitor 15 minute mean", colour="black")+
  theme_bw()+
  theme(axis.text.y = element_text(size=12),
        axis.title.y = element_text(size=14))

plot1

### check temperature correlation

ggplot()+
  geom_point(data=reference, aes(x=datetime, y=temp), colour="black")+
  geom_point(data=no2, aes(x=datetime, y=temp), colour="blue")



