########################################################
####        NO2 CORRECTION ALGORITHM BASED          ####
#### ON DATA FROM COLOCATION WITH REFERENCE STATION ####
########################################################

rm(list=ls()) # clear R's brain
setwd("~/GitHub/SmogOff/data") # set working directory (where data is stored)

library(tidyverse) # load tidyverse package
library(ggplot2) # load ggplot2 package
library(lubridate) # load lubridate package
library(scales) # load scales package 

data <- read.table("16.05.2022_colocation_Treliske.TXT", sep=",", skip=2, header=F) # read data

data <- data %>% 
  rename(datetime = V1,
         millisec_since_startup = V2,
         type = V3) %>%# rename columns
  mutate(datetime = ymd_hms(datetime, tz="Europe/London"),
         type = as.character(type))

data$type[which(data$type == " no2")] <- "no2" 
data$type[which(data$type == " pm")] <- "pm"
data <- data %>% 
  mutate(type = as.factor(type)) # clunky way to remove space before names of type

# filter out time after before co-located 
data <- data %>%
  filter(datetime >= min(datetime) + hours(2))


#### create no2 dataframe for converting back to uncorrected values
no2u <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
  rename(we_mv = V5, aux_mv = V6) %>% 
  mutate(uncorrectedNO2 = ((we_mv-218)-(aux_mv-250))/0.271) %>% 
  mutate(uncorrectedNO2 = uncorrectedNO2*1.9125)

# #### create no2 dataframe for converting algo3 to algo3 with no correction
# no2u3 <- data %>% filter(type=="no2") %>% select(-V4,-V7) %>% 
#   rename(we_mv = V5, aux_mv = V6) %>% 
#   mutate(uncorrectedAlgo3 = ((we_mv-218)-((208-218)-(241-250))-(aux_mv-250))/0.271) %>% 
#   mutate(uncorrectedAlgo3 = uncorrectedAlgo3*1.9125)


#### create no2 dataframe for converting to algorithm 1
no2a1 <- data %>% filter(type=="no2") %>% select(-V7) %>% 
  rename(we_mv = V5, aux_mv = V6, temp = V4) %>% 
  mutate(temp = round(temp, digits=1)) %>% 
  mutate(temp = as.character(temp))

# create dataframe using correction values based on Application Note AAN-803-05
a <- data.frame(temp = c(-30,-20,-10,0,10,20,30,40,50), correction=c(1.3,1.3,1.3,1.3,1,0.6,0.4,0.2,-1.5)) # From table 3 in application note document

##### 
func <- approxfun(x=a$temp, y=a$correction,method="linear", ties=mean)

corrected_val <- data.frame(n = seq(1,801,1),correction = func(seq(-30, 50, 0.1)))

b <- data.frame(n = seq(1,801,1),temp = seq(-30,50,0.1))


lookup <- left_join(x=b, y= corrected_val, by="n") %>% mutate(temp=as.character(temp))
lookup <- lookup %>% 
  mutate(temp = as.numeric(temp),
         temp = round(temp, digits= 1),
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))

plot <- ggplot()+
  geom_point(data=lookup,
             aes(x=temp,
                 y=correction), alpha = 0.25, colour =  "black")+
  geom_point(data=a,
             aes(x=temp,
                 y=correction), alpha = 1, size=5, colour =  "red")
plot

no2a1 <- no2a1 %>% 
  mutate(temp = as.numeric(temp), 
         temp_charactercode = temp*10,
         temp_charactercode = as.character(temp_charactercode))
no2a1 <- left_join(x=no2a1, y=lookup, by="temp_charactercode")
no2a1 <- no2a1 %>% select(-n) 

no2a1 <- no2a1 %>% 
  mutate(algo1 = ((we_mv-218)-(correction*(aux_mv-250)))/0.271) %>% 
  mutate(algo1 = algo1*1.9125)


## split to isolate correct reading for pm and no2 and then recombine back to one dataset
no2 <- data %>% 
  filter(type == "no2") %>%
  rename(we_mv = V5, aux_mv = V6,algo3 = V7,temp = V4)

no2a1 <- no2a1 %>% select(millisec_since_startup, algo1)
no2u <- no2u %>% select(millisec_since_startup, uncorrectedNO2)
# no2u3 <- no2u3 %>% select(millisec_since_startup, uncorrectedAlgo3)

no2 <- left_join(x=no2, y=no2a1, by="millisec_since_startup")
no2 <- left_join(x=no2, y=no2u, by="millisec_since_startup")
# no2 <- left_join(x=no2, y=no2u3, by="millisec_since_startup")

rm(no2a1, no2u, a, b, corrected_val, lookup)

### REFERENCE DATA
setwd("U:/DOCTORAL CAREER DEVELOPMENT FUND")
reference <- read.csv("treliskedata_16.05.2022.csv")

reference <- reference %>% 
  mutate(Date=as.character(Date),
         Time = as.character(Time))

reference$Time <- ifelse(reference$Time == "24:00:00", "00:00", reference$Time)

reference <- reference %>% 
  mutate(datetime = paste(Date, Time, sep=" "))

reference$datetime <- ifelse(reference$datetime == "16/05/2022 00:00", "17/05/2022 00:00", reference$datetime)

reference <- reference %>% 
  mutate(datetime = dmy_hm(datetime,  tz="Europe/London"))


# 15 minute mean

time_intervals <- reference %>% select(datetime) %>% rename(datetime_start = datetime) %>% 
  mutate(datetime_end = datetime_start + (15*60)-1)

### LOOP --> FOR EACH DATA POINT IN "no2" (i.e. SMOGOFF) DATA, RETURN THE TIME INTERVAL THAT IT BELONGS TO ###

data_list <- as.list(NA) # list to store data

# progress bar
pbar <- txtProgressBar(max = nrow(no2), style = 3)

for(i in 1:nrow(no2)) { 
  interval <- time_intervals$datetime_start[which(no2$datetime[i] %within% interval(time_intervals$datetime_start, time_intervals$datetime_end))]
  df <- merge(x = no2[i,], y = data.frame(interval))
  data_list[[i]] <- df  
  setTxtProgressBar(pb = pbar, value = i)
}

library(data.table)
new_data <- rbindlist(data_list) 
rm(data_list, pbar, df)

no2 <- new_data
rm(new_data)

### 15 minute mean
mean <- no2 %>% 
  group_by(interval) %>% 
  summarise(max = max(algo3),
            median = median(algo3),
            q10 = quantile(algo3, 0.10),
            q11 = quantile(algo3, 0.11),
            q12 = quantile(algo3, 0.12),
            q13 = quantile(algo3, 0.13), 
            q14 = quantile(algo3, 0.14),
            q15 = quantile(algo3, 0.15),
            q20 = quantile(algo3, 0.20),)

#### TESTING REMOVING NEGATIVES FOLLOWING OPTIMISING MATCH USING QUANTILES

# set remaining negatives to 0
mean <- mean %>% 
  mutate(zeroed = ifelse(q13 <0, 0, q13)) # chose quantile 39% because gave best match after zeroing (based on mean across data range)

mean <- mean %>% 
  mutate(interval = interval + (15*60))
no2 <- no2 %>% 
  mutate(interval = interval + (15*60))

### PLOT ### 

plot1 <- ggplot()+
  
  geom_point(data=reference,
             aes(x=datetime,
                 y=NO2ugm3), alpha = 1, colour =  "black")+
  geom_point(data=no2,
             aes(x=datetime,
                 y=algo3), alpha = 0.25, colour =  "skyblue")+
  geom_point(data = mean,
             aes(x=interval,
                 y=zeroed), alpha = 1, colour = "blue")+
  
  geom_line(data=mean, aes(x=interval, y=zeroed), colour="blue", size=1)+
  geom_line(data=reference, aes(x=datetime, y=NO2ugm3), colour="black", size=1)+
  
  scale_y_continuous(name = expression(paste(NO[2], " concentration (\u03bcg/ ", "m"^"3", ")", sep=" ")), breaks = seq(-200,300,10), limits = c(-50,120))+
  scale_x_datetime(breaks = date_breaks("3 hour"), labels = date_format("%H:%M", tz="Europe/London"))+
  xlab("")+
  annotate("text", x = ymd_hms("2022-05-16 10:00:00"), y = 70, label = "NO2", size=8)+
  annotate("text", x = ymd_hms("2022-05-17 03:00:00"), y = 70, label = "SmogOff 15 minute mean", colour="darkblue")+
  annotate("text", x = ymd_hms("2022-05-17 03:00:00"), y = 65, label = "Treliske reference monitor 15 minute mean", colour="black")+
  theme_bw()+
  theme(axis.text.y = element_text(size=12),
        axis.title.y = element_text(size=14))

plot1

mean(mean$zeroed)
reffilter <- reference %>% filter(datetime >= min(mean$interval) & datetime <= max(mean$interval))
mean(reffilter$NO2ugm3)
