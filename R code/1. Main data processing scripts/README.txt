This folder contains R scripts to clean and plot SmogOff data.

# NO2.R is for processing and plotting NO2 data 
--> with data-correction option based on our colocation with a Cornwall Council reference station

# PM.R is for processing and plotting PM data
--> with data-correction option for PM10 but not PM2.5 as reference station only measured PM10.